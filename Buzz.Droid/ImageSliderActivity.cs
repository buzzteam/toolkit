using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Buzz.Droid.CustomViews;
using Android.Support.V4.App;

using FragmentManager = Android.Support.V4.App.FragmentManager;
using FragmentActivity = Android.Support.V4.App.FragmentActivity;

namespace Buzz.Droid
{
	[Activity (Label = "ImageSliderActivity")]			
	public class ImageSliderActivity : FragmentActivity
	{
		private ImageSliderView _imageSliderView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.ActivityImageSlider);

			_imageSliderView = FindViewById<ImageSliderView> (Resource.Id.ImageSlider);

			_imageSliderView.HeightRatio = 1f;
			_imageSliderView.SetImages (SupportFragmentManager, Constants.imageList.ToList());

			//	_imageSliderView.SetImages (SupportFragmentManager, Constants.ImageProviderTest());

		}
	}
}

