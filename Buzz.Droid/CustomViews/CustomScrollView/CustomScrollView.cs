using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace Buzz.Droid.CustomViews
{
	class CustomScrollView : ScrollView
	{
		private float xDistance, yDistance, xLast, yLast;

		public CustomScrollView(Context context, IAttributeSet attrs) : base(context, attrs)
		{

		}

		public CustomScrollView(Context context) : base(context)
		{

		}

		public override bool OnInterceptTouchEvent (MotionEvent ev)
		{
			switch (ev.Action) {
			case MotionEventActions.Down:
				xDistance = yDistance = 0f;
				xLast = ev.GetX ();
				yLast = ev.GetY ();
				break;
			case MotionEventActions.Move:
				float curX = ev.GetX ();
				float curY = ev.GetY ();
				xDistance += Math.Abs (curX - xLast);
				yDistance += Math.Abs (curY - yLast);
				if (xDistance > yDistance)
					return false;
				break;
			}

			return base.OnInterceptTouchEvent (ev);
		}
	}
}

