using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Buzz.Droid.CustomViews;
using Buzz.Core.Exceptions;
using Buzz.Droid.Utils;


using FragmentManager = Android.Support.V4.App.FragmentManager;
using Fragment = Android.Support.V4.App.Fragment;

namespace Buzz.Droid.CustomViews
{
	public class ImageSliderViewFragment : Fragment
	{
		private ImageLoadingIndicatorView _imageLoadingIndicatorView;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);

			// Create your fragment here
		}

		public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			var layout = new RelativeLayout (Activity);

			_imageLoadingIndicatorView = new ImageLoadingIndicatorView(Activity);

			_imageLoadingIndicatorView.FadeDuration = Arguments.GetInt ("fadeDuration", 200);
			_imageLoadingIndicatorView.ImageWidth = Arguments.GetInt ("imageWidth", 0);
			_imageLoadingIndicatorView.ImageHeight = Arguments.GetInt ("imageHeight", 0);
			_imageLoadingIndicatorView.LowQuality = Arguments.GetBoolean ("_lowQuality", false);
			_imageLoadingIndicatorView.DeQueueType = (ImageLoaderView.deQueueType) Arguments.GetInt ("deQueueType", (int)ImageLoaderView.deQueueType.Download);
			_imageLoadingIndicatorView.ScaleType = ImageView.ScaleType.ValueOf(Arguments.GetString ("scaleType"));

			_imageLoadingIndicatorView.Url = Arguments.GetString ("url");

			layout.AddView (_imageLoadingIndicatorView, LayoutParamsHelper.Relative.MatchParentParams);
			return layout;
		}

		public override void OnDestroyView ()
		{
			_imageLoadingIndicatorView.Dispose ();
			base.OnDestroyView ();
		}

		/*public override void OnDestroy ()
		{
			_imageLoadingIndicatorView.Dispose ();
			base.OnDestroy ();
		}*/
	}
}

