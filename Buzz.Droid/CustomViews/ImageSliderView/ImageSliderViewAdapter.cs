using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.App;

using FragmentManager = Android.Support.V4.App.FragmentManager;
using Fragment = Android.Support.V4.App.Fragment;

using Buzz.Core.Contracts;
using Buzz.Droid.AL;

namespace Buzz.Droid.CustomViews
{
	class ImageSliderViewAdapter : FragmentStatePagerAdapter
	{
		private IImageMetaDataProvider _imageMetaDataProvider;
		private IList<string> _urls;

		private int _fadeDuration;
		private int _imageWidth;
		private int _imageHeight;
		private bool _lowQuality;
		private ImageLoaderView.deQueueType _deQueueType;
		private ImageView.ScaleType _scaleType;

		public ImageSliderViewAdapter(FragmentManager fm, IImageMetaDataProvider provider, int fadeDuration, int imageWidth, int imageHeight, bool lowQuality, ImageLoaderView.deQueueType deQueueType, ImageView.ScaleType scaleType) : base(fm)
		{
			_imageMetaDataProvider = provider;
			Construct (fadeDuration, imageWidth, imageHeight, lowQuality, deQueueType, scaleType);
		}

		public ImageSliderViewAdapter(FragmentManager fm, IList<string> urls, int fadeDuration, int imageWidth, int imageHeight, bool lowQuality, ImageLoaderView.deQueueType deQueueType, ImageView.ScaleType scaleType) : base(fm)
		{
			_urls = urls;
			Construct (fadeDuration, imageWidth, imageHeight, lowQuality, deQueueType, scaleType);
		}

		public void Construct(int fadeDuration, int imageWidth, int imageHeight, bool lowQuality, ImageLoaderView.deQueueType deQueueType, ImageView.ScaleType scaleType) {
			_fadeDuration = fadeDuration;
			_imageWidth = imageWidth;
			_imageHeight = imageHeight;
			_lowQuality = lowQuality;
			_deQueueType = deQueueType;
			_scaleType = scaleType;
		}

		public override int Count {
			get {
				if (_imageMetaDataProvider != null)
					return _imageMetaDataProvider.ImageMetaData.Count;
				else if (_urls != null)
					return _urls.Count;
				else
					return 0;
			}
		}


		public override Fragment GetItem (int position)
		{
			ImageSliderViewFragment fragment = new ImageSliderViewFragment ();
			Bundle bundle = new Bundle ();

			if (_imageMetaDataProvider != null)
				bundle.PutString ("url", _imageMetaDataProvider.ImageMetaData [position].Url);
			else if (_urls != null)
				bundle.PutString ("url", _urls [position]);

			bundle.PutInt ("fadeDuration", _fadeDuration);
			bundle.PutInt ("imageWidth", _imageWidth);
			bundle.PutInt ("imageHeight", _imageHeight);
			bundle.PutBoolean ("_lowQuality", _lowQuality);
			bundle.PutInt ("deQueueType", (int)_deQueueType);
			bundle.PutString ("scaleType", _scaleType.ToString());

			fragment.Arguments = bundle;
			return fragment;
		}
	}
}