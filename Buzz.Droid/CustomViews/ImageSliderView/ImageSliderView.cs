using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V4.View;
using Android.Util;
using Android.Content.Res;
using Android.Support.V4.App;

using FragmentManager = Android.Support.V4.App.FragmentManager;
using Fragment = Android.Support.V4.App.Fragment;

using Buzz.Core.Exceptions;
using Buzz.Droid.Utils;
using Buzz.Core.Contracts;
using Buzz.Droid.AL;

namespace Buzz.Droid.CustomViews
{
	class ImageSliderView : RelativeLayout
	{
		private IImageMetaDataProvider _provider;
		private IList<string> _urls;

		private	Context _context;
		private ViewPager _viewPager;
		private LinearLayout _linearLayout;
		private ImageView[] indicators;
		private FragmentManager _fragmentManager;
		private int _currentItem = 0;
		private float _heightRatio;

		public int FadeDuration = 200;
		public int ImageWidth = 0;
		public int ImageHeight = 0;
		public bool LowQuality = false;
		public ImageLoaderView.deQueueType DeQueueType = ImageLoaderView.deQueueType.Download;
		public ImageView.ScaleType ScaleType = ImageView.ScaleType.CenterInside;

		public float HeightRatio {
			set {
				_heightRatio = value;
				if (MeasuredWidth > 0) {
					LayoutParameters.Height = (int)Math.Round (MeasuredWidth * _heightRatio);
				}
			}
		}

		public ImageSliderView(Context context) : base(context)
		{
			_context = context;
			LayoutSubviews ();
		}

		public ImageSliderView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			_context = context;
			LayoutSubviews ();

			TypedArray a = _context.ObtainStyledAttributes (attrs, Resource.Styleable.ImageSliderView);

			int N = a.IndexCount;
			for (int i = 0; i < N; ++i)
			{
				int attr = a.GetIndex(i);
				switch (attr)
				{
				case Resource.Styleable.ImageSliderView_fadeDuration:
					int fadeDuration = a.GetInt (attr, 200);
					FadeDuration = fadeDuration;
					break;
				case Resource.Styleable.ImageSliderView_deQueueType:
					string deQueue = a.GetString (attr);
					if (deQueue == "request") {
						DeQueueType = ImageLoaderView.deQueueType.Request;
					} else {
						DeQueueType = ImageLoaderView.deQueueType.Download;
					}
					break;
				}
			}
			a.Recycle ();

		}


		private void LayoutSubviews() {
			_viewPager = new ViewPager (_context);
			_viewPager.Id = 99;

			_viewPager.PageSelected += (object sender, ViewPager.PageSelectedEventArgs e) => {
				SetIndicators( e.Position);
			};

			_linearLayout = new LinearLayout (_context);

			AddView (_viewPager, LayoutParamsHelper.Relative.MatchParentParams);
			AddView (_linearLayout, LayoutParamsHelper.Relative.CenterBottomInParentParams);
		}

		public void SetImages(FragmentManager fm, IImageMetaDataProvider provider) {
			_provider = provider;
			int imageCount = _provider.ImageMetaData.Count;
			if (imageCount > 0) {
				_viewPager.Adapter = new ImageSliderViewAdapter (fm, _provider, FadeDuration, ImageWidth, ImageHeight, LowQuality, DeQueueType, ScaleType);
				SetImages (imageCount);
			}
		}

		public void SetImages(FragmentManager fm, IList<string> urls) {
			_urls = urls;
			int imageCount = _urls.Count;
			if (imageCount > 0) {
				_viewPager.Adapter = new ImageSliderViewAdapter (fm, _urls, FadeDuration, ImageWidth, ImageHeight, LowQuality, DeQueueType, ScaleType);
				SetImages (imageCount);
			}
		}

		public void SetImages(int imageCount) {
			if (imageCount > 1) {
				indicators = new ImageView[imageCount];
				int size = AndroidHelper.GetPixels(8);
				int padding = AndroidHelper.GetPixels(4);

				for (int i = 0; i < imageCount; i++) 
				{
					ImageView item = new ImageView (_context);
					item.SetImageResource(Resource.Drawable.ImageSliderViewIndicatorUnselected);
					item.LayoutParameters = new ViewGroup.LayoutParams (size+padding*2, size+padding*2);
					item.SetScaleType (ImageView.ScaleType.CenterInside);
					item.SetPadding (padding, 0, padding, 0);
					_linearLayout.AddView (item);
					indicators[i] = item;
				}
				SetIndicators(_currentItem);
			}
		}

		public void SetIndicators(int position)
		{
			indicators [_currentItem].SetImageResource (Resource.Drawable.ImageSliderViewIndicatorUnselected);
			indicators [position].SetImageResource (Resource.Drawable.ImageSliderViewIndicatorSelected);
			_currentItem = position;
		}
			
		private FragmentManager FragmentManager {
			get {
				if (_fragmentManager != null) {
					return _fragmentManager;
				} else {
					throw new NullFragmentManagerException ();
				}
			}
			set {
				_fragmentManager = value;
			}
		}

		public ImageSliderViewAdapter Adapter {
			get {
				if (_viewPager != null) {
					return (ImageSliderViewAdapter)_viewPager.Adapter;
				} else {
					throw new NullViewPagerException ();
				}
			}
			set {
				if (_viewPager != null) {
					_viewPager.Adapter = null;
				}
			}
		}

		protected override void OnSizeChanged (int w, int h, int oldw, int oldh)
		{
			base.OnSizeChanged (w, h, oldw, oldh);
			if (_heightRatio > 0)
				HeightRatio = _heightRatio;
		}
	}
}

