using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Buzz.Core.Utils;
using Buzz.Droid.AL;
using Android.Graphics.Drawables;
using MonoTouch.Dialog.Utilities;
using Android.Graphics;
using Android.Content.Res;

/// <summary>
/// Add this to the top of the layout file to use custom attributes
/// xmlns:custom="http://schemas.android.com/apk/res/uk.co.buzzinteractive.toolkit"
/// 
/// Custom attributes:
/// fadeDuration - set in milliseconds
/// deQueueType - set to "request" or "download in XML - deQueueType.Request or deQueueType.Download in code
/// </summary>
/// 
using System.Threading;
using Android.Views.Animations;
using Buzz.Core.Exceptions;
using Buzz.Droid.Utils;


namespace Buzz.Droid.CustomViews
{
	class ImageLoaderView : ImageView, IImageUpdated
	{
		private bool _isDisposed = false;
		private string _uri;
		private Activity _context;
		private Animation _fadeIn;

		public event EventHandler ImageLoaded;
		public event EventHandler ImageWillLoad;
		public int FadeDuration;
		public int ImageWidth = 0;
		public int ImageHeight = 0;
		public bool LowQuality = false;
		public deQueueType DeQueueType = deQueueType.Download;

		private bool postHoneycomb;

		public enum deQueueType
		{
			Request,
			Download
		};

		public ImageLoaderView(Context context) : base(context)
		{
			_context = (Activity)context;
			postHoneycomb = (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Honeycomb);
		}

		public ImageLoaderView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			_context = (Activity)context;
			postHoneycomb = (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Honeycomb);

			TypedArray a = _context.ObtainStyledAttributes (attrs, Resource.Styleable.ImageLoaderView);

			int N = a.IndexCount;
			for (int i = 0; i < N; ++i)
			{
				int attr = a.GetIndex(i);
				switch (attr)
				{
				case Resource.Styleable.ImageLoaderView_fadeDuration:
					int fadeDuration = a.GetInt (attr, 200);
					FadeDuration = fadeDuration;
					break;

				case Resource.Styleable.ImageLoaderView_deQueueType:
					string deQueue = a.GetString (attr);
					if (deQueue == "request") {
						DeQueueType = deQueueType.Request;
					} else {
						DeQueueType = deQueueType.Download;
					}
					break;
				}
			}
			a.Recycle ();
		}

		//Set the Image Url here and it will be loaded into the ImageView asynchronously
		public string Url {
			set {

				if (_uri != null) {
					if (_uri == value)
						return;
					DeQueue (_uri, this);
				}

				if (ImageWidth == 0 || ImageHeight == 0) {
					ImageWidth = AndroidHelper.GetScreenWidth ();
					ImageHeight = AndroidHelper.GetScreenHeight ();
				}

				SetImageBitmap (null);
				if (ImageWillLoad != null) {
					ImageWillLoad (this, new EventArgs ());
				}

				ThreadPool.QueueUserWorkItem (delegate { 
					Bitmap bitmap = ImageManager.Instance.GetImage (value, this, ImageWidth, ImageHeight, LowQuality);
					if (bitmap != null) {
						_fadeIn = null;
						((Activity)_context).RunOnUiThread (() => {
							// no setAlpha pre API 11
							//    Alpha = 1f;
							SetImage (bitmap);
						});
					}
				});

				_uri = value;
			}
			get {
				return _uri;
			}

		}

		public void UpdatedImage(Uri uri)
		{
			LogHelper.LogDebug ("Image updated {0}", uri.AbsoluteUri);

			if (uri.AbsoluteUri == _uri) {

				Bitmap bitmap = ImageManager.Instance.GetImage (uri.AbsoluteUri, this, ImageWidth, ImageHeight, LowQuality);
				if (bitmap != null && !_isDisposed) {

					if (this.Drawable == null && postHoneycomb) {
						_fadeIn = new AlphaAnimation (0, 1);
						_fadeIn.Duration = FadeDuration;
					}

					((Activity)_context).RunOnUiThread (() => {
						SetImage (bitmap);
					});
				}
			}
		}

		public void SetImage(Bitmap bitmap) {
			// Check to see if there is an ImageLoaded EventHandler, if so then call it
			if (ImageLoaded != null) {
				ImageLoaded (this, new EventArgs ());
			}

			if (_fadeIn != null && !_isDisposed) {
				this.Animation = _fadeIn;
			}

			if (!_isDisposed)
				this.SetImageBitmap (bitmap);

			bitmap.Dispose ();

		}

		protected override void Dispose (bool disposing)
		{
			_isDisposed = true;

			if (_uri != null) {
				DeQueue (_uri, this);
			}

			this.SetImageBitmap (null);

			base.Dispose (disposing);
		}

		private void DeQueue(string uri, IImageUpdated notify) {
			if (DeQueueType == deQueueType.Request) {
				ImageManager.Instance.DeQueueRequest (_uri, notify);
			} else if (DeQueueType == deQueueType.Download) {
				ImageManager.Instance.DeQueueDownload (_uri, notify);
			} else {
				throw new BadDeQueueTypeException ();
			}
		}
	}
}

