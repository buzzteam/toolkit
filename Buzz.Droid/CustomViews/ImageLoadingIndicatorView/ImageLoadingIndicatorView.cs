using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Buzz.Core.Exceptions;
using Android.Content.Res;


/// <summary>
/// Add this to the top of the layout file to use custom attributes
/// xmlns:custom="http://schemas.android.com/apk/res/uk.co.buzzinteractive.toolkit"
/// 
/// Custom attributes:
/// fadeDuration - set in milliseconds
/// deQueueType - set to "request" or "download in XML - deQueueType.Request or deQueueType.Download in code
/// </summary>
/// 
using Buzz.Droid.Utils;

namespace Buzz.Droid.CustomViews
{
	class ImageLoadingIndicatorView : RelativeLayout
	{
		private Context _context;
		private ProgressBar _progressBar;
		private ImageLoaderView _imageLoaderView;
		private EventHandler _imageLoadedHandler;
		private EventHandler _imageWillLoadHandler;

		object locker = new object();

		public ImageLoadingIndicatorView(Context context) : base(context)
		{
			_context = context;
			LayoutSubviews ();
		}

		public ImageLoadingIndicatorView(Context context, IAttributeSet attrs) : base(context, attrs)
		{
			_context = context;
			LayoutSubviews ();

			TypedArray a = _context.ObtainStyledAttributes (attrs, Resource.Styleable.ImageLoadingIndicatorView);

			int N = a.IndexCount;
			for (int i = 0; i < N; ++i)
			{
				int attr = a.GetIndex(i);
				switch (attr)
				{
				case Resource.Styleable.ImageLoadingIndicatorView_fadeDuration:
					int fadeDuration = a.GetInt (attr, 200);
					_imageLoaderView.FadeDuration = fadeDuration;
					break;

				case Resource.Styleable.ImageLoadingIndicatorView_deQueueType:
					string deQueue = a.GetString (attr);
					if (deQueue == "request") {
						DeQueueType = ImageLoaderView.deQueueType.Request;
					} else {
						DeQueueType = ImageLoaderView.deQueueType.Download;
					}
					break;
				}
			}
			a.Recycle ();

		}

		private void LayoutSubviews() {
			_progressBar = new ProgressBar (_context, null, Android.Resource.Attribute.ProgressBarStyle);
			_progressBar.Indeterminate = true;
			_progressBar.Visibility = ViewStates.Visible;

			_imageWillLoadHandler = delegate {
				if (_progressBar != null) {
					_progressBar.Visibility = ViewStates.Visible;
				}
			};

			_imageLoadedHandler = delegate {
				if (_progressBar != null) {
					_progressBar.Visibility = ViewStates.Invisible;
				}
			};

			_imageLoaderView = new ImageLoaderView(_context);
			_imageLoaderView.ImageWillLoad += _imageWillLoadHandler;
			_imageLoaderView.ImageLoaded += _imageLoadedHandler;

			AddView (_imageLoaderView, LayoutParamsHelper.Relative.MatchParentParams);
			AddView (_progressBar, LayoutParamsHelper.Relative.CenterInParentParams);
		}

		public string Url {
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.Url = value;
				}
			}
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.Url;
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
		}

		public event EventHandler ImageLoaded {
			add {
				lock (locker) {
					if (_imageLoaderView != null) {
						_imageLoaderView.ImageLoaded += value;
					}
				}
			}
			remove {
				lock (locker) {
					if (_imageLoaderView != null) {
						_imageLoaderView.ImageLoaded -= value;
					}
				}
			}
		}
		public int FadeDuration {
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.FadeDuration;
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.FadeDuration = value;
				}
			}
		}

		public ImageView.ScaleType ScaleType {
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.GetScaleType();
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.SetScaleType(value);
				}
			}
		}

		public ImageLoaderView.deQueueType DeQueueType {
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.DeQueueType;
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.DeQueueType = value;
				}
			}
		}

		public int ImageWidth {
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.ImageWidth;
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.ImageWidth= value;
				}
			}
		}

		public int ImageHeight {
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.ImageHeight;
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.ImageHeight = value;
				}
			}
		}

		public bool LowQuality {
			get {
				if (_imageLoaderView != null) {
					return _imageLoaderView.LowQuality;
				} else {
					throw new NullImageLoaderViewException ();
				}
			}
			set {
				if (_imageLoaderView != null) {
					_imageLoaderView.LowQuality = value;
				}
			}
		}

		protected override void Dispose (bool disposing)
		{
			_imageLoaderView.ImageLoaded -= _imageLoadedHandler;
			_imageLoaderView.ImageWillLoad -= _imageWillLoadHandler;

			_imageLoaderView.Dispose();
			_progressBar.Dispose ();

			base.Dispose (disposing);
		}
	}
}

