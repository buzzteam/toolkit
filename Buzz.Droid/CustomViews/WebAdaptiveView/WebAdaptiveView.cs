﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Webkit;
using Android.Graphics;
using Android.Util;
using Buzz.Droid.Utils;
using Java.Interop;
using System.Threading;



namespace Buzz.Droid.CustomViews
{
    public class WebAdaptiveView : RelativeLayout
    {     
        public ExpandableWebView InternalWebView;
        private WebAdaptiveViewActivity _context;
        private JsInterface jsInterface;
        private DisplayMetrics _displayMetrics;

        public double pixelDensityOffset;
       
        public WebAdaptiveView(WebAdaptiveViewActivity context) : base (context)
        {
            this._context = context;
            Construct ();
        }

        public WebAdaptiveView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
            this._context = (WebAdaptiveViewActivity) context;
            Construct ();
        }

        private void Construct()
        {
            this.SetBackgroundColor (Color.Transparent);

            _displayMetrics = Android.Content.Res.Resources.System.DisplayMetrics;

            jsInterface = new JsInterface (_context, this);
            RelativeLayout.LayoutParams rlParams = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, _displayMetrics.HeightPixels);

            InternalWebView = new ExpandableWebView (_context);
            InternalWebView.AddJavascriptInterface (jsInterface, "Android");
            InternalWebView.SetWebChromeClient (new WebChromeClient ());
            InternalWebView.Settings.JavaScriptEnabled = true;
            InternalWebView.LayoutParameters = rlParams;
            InternalWebView.VerticalScrollBarEnabled = false;
            InternalWebView.setExpanded(true);

            this.AddView (InternalWebView);

            pixelDensityOffset = _displayMetrics.Density - 1;
            Log.Debug ("Pixel Density Offset: ", pixelDensityOffset.ToString());
     
        }

        public void WillShrink()
        {
            this.LayoutParameters.Height = InternalWebView.Height;
            this.RequestLayout();

            InternalWebView.setExpanded (false);

            InternalWebView.LayoutParameters.Height = 1;//_displayMetrics.HeightPixels;
            InternalWebView.RequestLayout ();
        }

        public void DidResize()
        {
            InternalWebView.LayoutParameters.Height = InternalWebView.ContentHeight;
            InternalWebView.RequestLayout ();

            InternalWebView.setExpanded (true);

            this.LayoutParameters.Height = (int)(InternalWebView.ContentHeight + InternalWebView.ContentHeight * pixelDensityOffset);
            this.RequestLayout();
           
        }

        public void LoadDataWithBaseURL(string baseUrl, string data, string mimeType, string encoding, string failUrl)
        {
            InternalWebView.LoadDataWithBaseURL (baseUrl, data, mimeType, encoding, failUrl);
        }

        private class JsInterface : Java.Lang.Object
        {
            WebAdaptiveViewActivity _context;
            WebAdaptiveView _wav;

            public JsInterface (WebAdaptiveViewActivity context, WebAdaptiveView wav)
            {
                this._context = context;
                this._wav = wav;
            }

            [Export ("WillShrink")]
            [JavascriptInterface]
            public void WillShrink ()
            {
                _context.RunOnUiThread 
                (() => 
                {
                    _wav.WillShrink();
                }); 
            }

            [Export ("DidResize")]
            [JavascriptInterface]
            public void DidResize()
            {
                _context.RunOnUiThread 
                (() => 
                {
                    _wav.DidResize();
                }); 
            }
        }
    }
}

