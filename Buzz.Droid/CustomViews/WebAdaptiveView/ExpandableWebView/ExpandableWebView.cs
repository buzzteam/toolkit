using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Android.Webkit;

namespace Buzz.Droid.CustomViews
{
	public class ExpandableWebView : WebView
	{
		bool expanded = false;

		public ExpandableWebView(Context context):base(context)
		{

		}

		public ExpandableWebView(Context context, IAttributeSet attrs ):base(context, attrs)
		{

		}

		public ExpandableWebView(Context context, IAttributeSet attrs, int defStyle ):base(context, attrs, defStyle)
		{

		}

		public ExpandableWebView(Context context, IAttributeSet attrs, int defStyle, bool privateBrowsing ):base(context, attrs, defStyle)
		{

		}

		public bool isExpanded()
		{
			return expanded;
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			if (isExpanded ()) 
			{
				int expandSpec = MeasureSpec.MakeMeasureSpec (MeasuredSizeMask, MeasureSpecMode.AtMost);
				base.OnMeasure (widthMeasureSpec, expandSpec);
			} 
			else
				base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
		}

		public void setExpanded (bool expanded)
		{
			this.expanded = expanded;
		}
	}
}

