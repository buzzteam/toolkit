using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace Buzz.Droid.CustomViews
{
	public class ExpandableGridView : GridView
	{
		bool expanded = false;

		public ExpandableGridView(Context context):base(context)
		{

		}

		public ExpandableGridView(Context context, IAttributeSet attrs ):base(context, attrs)
		{

		}

		public ExpandableGridView(Context context, IAttributeSet attrs, int defStyle ):base(context, attrs, defStyle)
		{

		}

		public bool isExpanded()
		{
			return expanded;
		}

		protected override void OnMeasure (int widthMeasureSpec, int heightMeasureSpec)
		{
			if (isExpanded ()) 
			{
				int expandSpec = MeasureSpec.MakeMeasureSpec (MeasuredSizeMask, MeasureSpecMode.AtMost);
				base.OnMeasure (widthMeasureSpec, expandSpec);
			} 
			else
				base.OnMeasure (widthMeasureSpec, heightMeasureSpec);
		}

		public void SetExpanded (bool expanded)
		{
			this.expanded = expanded;
		}

	}
}

