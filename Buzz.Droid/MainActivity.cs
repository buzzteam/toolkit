using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Buzz.Droid
{
	[Activity (MainLauncher = true)]
	public class MainActivity : Activity
	{
		int count = 1;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate {
				button.Text = string.Format ("{0} clicks!", count++);
			};

			Button imageLoaderButton = FindViewById<Button> (Resource.Id.ImageLoaderButton);
			Button imageLoadingIndicatorButton = FindViewById<Button> (Resource.Id.ImageLoadingIndicatorButton);
			Button imageSlider = FindViewById<Button> (Resource.Id.ImageSliderButton);
			Button testButton = FindViewById<Button> (Resource.Id.testButton);
			Button adaptiveWebview = FindViewById<Button> (Resource.Id.btnAdaptiveWebView);
			Button reachability = FindViewById<Button> (Resource.Id.ReachabilityButton);
			Button qr = FindViewById<Button> (Resource.Id.QRButton);
			Button push = FindViewById<Button> (Resource.Id.PushButton);

			imageLoaderButton.Click += delegate {
				StartActivity(typeof(ImageLoaderActivity));
			};

			imageLoadingIndicatorButton.Click += delegate {
				StartActivity(typeof(ImageLoadingIndicatorActivity));
			};

			imageSlider.Click += delegate {
				StartActivity(typeof(ImageSliderActivity));
			};

			testButton.Click += delegate {
				StartActivity(typeof(TestActivity));
			};

            adaptiveWebview.Click += delegate {
                StartActivity(typeof(WebAdaptiveViewActivity));
			};

			reachability.Click += delegate {
				StartActivity(typeof(ReachabilityActivity));
			};

			qr.Click += delegate {
				StartActivity(typeof(QRActivity));
			};

			push.Click += delegate {
				StartActivity(typeof(PushActivity));
			};
		}
	}
}


