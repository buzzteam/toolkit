﻿using System;
using Buzz.Core.Contracts;
using Android.Net;
using Android.Content;

namespace Buzz.Droid.AL
{
	/// <summary>
	/// Reachability service.
	/// 
	/// 
	/// Requires Android permisson ACCESS_NETWORK_STATE
	/// </summary>
	public class ReachabilityService : IReachabilityService
	{
		public ReachabilityService ()
		{
		}

		public NetworkStatus InternetConnectionStatus ()
		{
			var systemService = ServiceContainer.Resolve<ISystemService> ();
			var context = (Context)systemService.GetApplicationContext ();

			var connectivityManager = (ConnectivityManager)context.GetSystemService(Context.ConnectivityService);

			if (connectivityManager.GetNetworkInfo(ConnectivityType.Wifi).GetState() == NetworkInfo.State.Connected) {
				return NetworkStatus.ReachableViaWiFiNetwork;
			} else if (connectivityManager.GetNetworkInfo(ConnectivityType.Mobile).GetState() == NetworkInfo.State.Connected) {
				return NetworkStatus.ReachableViaCarrierDataNetwork;
			}
			return NetworkStatus.NotReachable;
		}
	}

}