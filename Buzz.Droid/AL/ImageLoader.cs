//this is a modified version.  Included a prioritised queue to allow background caching and correct a few bugs


// Copyright 2010-2011 Miguel de Icaza
//
// Based on the TweetStation specific ImageStore
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

using MonoTouch.Dialog.Utilities;
using System.Security.Cryptography;
using Android.App;
using Android.Graphics.Drawables;
using Android.Util;
using Buzz.Core.Utils;
using Android.Graphics;
using Buzz.Droid.AL;
using Buzz.Droid.Utils;

namespace MonoTouch.Dialog.Utilities 
{
	/// <summary>
	///    This interface needs to be implemented to be notified when an image
	///    has been downloaded.   The notification will happen on the UI thread.
	///    Upon notification, the code should call RequestImage again, this time
	///    the image will be loaded from the on-disk cache or the in-memory cache.
	/// </summary>
	public interface IImageUpdated {
		void UpdatedImage (Uri uri);
	}

	/// <summary>
	///   Network image loader, with local file system cache and in-memory cache
	/// </summary>
	/// <remarks>
	///   By default, using the static public methods will use an in-memory cache
	///   for 50 images and 4 megs total.   The behavior of the static methods 
	///   can be modified by setting the public DefaultLoader property to a value
	///   that the user configured.
	/// 
	///   The instance methods can be used to create different imageloader with 
	///   different properties.
	///  
	///   Keep in mind that the phone does not have a lot of memory, and using
	///   the cache with the unlimited value (0) even with a number of items in
	///   the cache can consume memory very quickly.
	/// 
	///   Use the Purge method to release all the memory kept in the caches on
	///   low memory conditions, or when the application is sent to the background.
	/// </remarks>

	public class ImageLoader 
	{

		public static string CacheFolderPath
		{
			get{
				if(Android.OS.Environment.ExternalStorageState == Android.OS.Environment.MediaMounted)
					return Application.Context.ExternalCacheDir.AbsolutePath;
				else
					return Application.Context.CacheDir.AbsolutePath;
			}
		}

		public readonly static string BaseDir = CacheFolderPath + "/";
		//public readonly static string BaseDir = Path.Combine (Environment.GetFolderPath (Environment.SpecialFolder.Personal), "..");
		const int MaxRequests = 3;
		const int MaxBackgroundRequests = 1;
		public static string PicDir = BaseDir; //System.IO.Path.Combine (BaseDir, "Pictures.MonoTouch.Dialog/");

		// Cache of recently used images
		LRUCache<Uri, Drawable /*		UIImage*/> cache;

		// A list of requests that have been issues, with a list of objects to notify.
		private static Dictionary<Uri, List<IImageUpdated>> pendingRequests;

		// A list of updates that have completed, we must notify the main thread about them.
		//	static HashSet<Uri> queuedUpdates;

		// A queue used to avoid flooding the network stack with HTTP requests
		//static Stack<Uri> requestQueue;
		private static IList<Uri> requestQueue;

		private static IList<Uri> currentDownloads;

		/*		static NSString nsDispatcher = "x"; */

		static MD5CryptoServiceProvider checksum = new MD5CryptoServiceProvider ();

		/// <summary>
		///    This contains the default loader which is configured to be 50 images
		///    up to 4 megs of memory.   Assigning to this property a new value will
		///    change the behavior.   This property is lazyly computed, the first time
		///    an image is requested.
		/// </summary>
		//public static ImageLoader DefaultLoader = new ImageLoader (50, 4*1024*1024);
		//public static ImageLoader DefaultLoader = new ImageLoader (50,0);

		static ImageLoader ()
		{

			//PicDir = Path.Combine (BaseDir, "Pictures.MonoTouch.Dialog/");

			if (!Directory.Exists (PicDir))
				Directory.CreateDirectory (PicDir);

			pendingRequests = new Dictionary<Uri,List<IImageUpdated>> ();
			//	queuedUpdates = new HashSet<Uri>();
			//requestQueue = new Stack<Uri> ();
			//requestQueue = new ImageLoaderStack();
			requestQueue = new List<Uri> ();
			currentDownloads = new List<Uri> ();
		}

		/// <summary>
		///   Creates a new instance of the image loader
		/// </summary>
		/// <param name="cacheSize">
		/// The maximum number of entries in the LRU cache
		/// </param>
		/// <param name="memoryLimit">
		/// The maximum number of bytes to consume by the image loader cache.
		/// </param>
		public ImageLoader (int cacheSize, int memoryLimit)
		{
			cache = new LRUCache<Uri, Drawable /*			UIImage*/> (cacheSize, memoryLimit, sizer);
		}



		static int sizer (Drawable /*		UIImage*/ img)
		{
			/*			var cg = img.CGImage;
            return cg.BytesPerRow * cg.Height;*/
			var pixels = img.IntrinsicHeight * img.IntrinsicWidth;
			return pixels * 3; //HACK: assume 3 bytes per pixel (24bit)
		}

		/// <summary>
		///    Purges the contents of the DefaultLoader
		/// </summary>
		/*	public static void Purge ()
		{
			if (DefaultLoader != null)
				DefaultLoader.PurgeCache ();
		}*/

		/// <summary>
		///    Purges the cache of this instance of the ImageLoader, releasing 
		///    all the memory used by the images in the caches.
		/// </summary>
		public void PurgeCache ()
		{
			lock (cache)
				cache.Purge ();
		}

		static int hex (int v)
		{
			if (v < 10)
				return '0' + v;
			return 'a' + v-10;
		}

		public static string md5 (string input)
		{
			var bytes = checksum.ComputeHash (Encoding.UTF8.GetBytes (input));
			var ret = new char [32];
			for (int i = 0; i < 16; i++){
				ret [i*2] = (char)hex (bytes [i] >> 4);
				ret [i*2+1] = (char)hex (bytes [i] & 0xf);
			}
			return new string (ret);
		}

		public void CacheImage(Uri uri)
		{
			RequestImage(uri, null);
		}

		/// <summary>
		///   Requests an image to be loaded using the default image loader
		/// </summary>
		/// <param name="uri">
		/// The URI for the image to load
		/// </param>
		/// <param name="notify">
		/// A class implementing the IImageUpdated interface that will be invoked when the image has been loaded
		/// </param>
		/// <returns>
		/// If the image has already been downloaded, or is in the cache, this will return the image as a UIImage.
		/// </returns>
		public static Bitmap DefaultRequestImage (Uri uri, IImageUpdated notify, int width = 0, int height = 0, bool lowQuality = false)
		{
			if(uri == null)
				return null;

			//	if (DefaultLoader == null)
			//	DefaultLoader = new ImageLoader (50, 4*1024*1024);

			return RequestImage (uri, notify, width, height, lowQuality);
		}

		/// <summary>
		///   Requests an image to be loaded from the network
		/// </summary>
		/// <param name="uri">
		/// The URI for the image to load
		/// </param>
		/// <param name="notify">
		/// A class implementing the IImageUpdated interface that will be invoked when the image has been loaded
		/// </param>
		/// <returns>
		/// If the image has already been downloaded, or is in the cache, this will return the image as a UIImage.
		/// </returns>



		public static Bitmap RequestImage (Uri uri, IImageUpdated notify, int width = 0, int height = 0, bool lowQuality = false)
		{
			Bitmap ret;

			//lock (currentDownloads) {
			//		if (currentDownloads.Contains(uri);
			//}



			string picfile = uri.IsFile ? uri.LocalPath : PicDir + md5 (uri.AbsoluteUri);
			if (File.Exists (picfile)){
				//				FileInfo info = new FileInfo (picfile);
				//				if (info.Length == 0) {
				//					File.Delete (picfile);
				//				} else {
				if (width == 0 || height == 0) {
					ret = BitmapFactory.DecodeFile (picfile); // Drawable.CreateFromPath(picfile); /*	
				} else {
					ret = AndroidHelper.DecodeSampledBitmapFromPath (picfile, width, height, lowQuality);
				}

				if (ret != null) {
					//lock (cache)
					//cache [uri] = ret;
					//Console.Error.WriteLine ("Returning from file {0}", uri.ToString());
					return ret;
				}
				//	}
			} 

			if (notify != null) {
				lock (pendingRequests){
					if (pendingRequests.ContainsKey (uri)) {
						if (!pendingRequests [uri].Contains (notify)) {
							pendingRequests [uri].Add (notify);

							LogHelper.LogDebug ("Adding callback to existing download");
						}
						return null;
					} else {
						var slot = new List<IImageUpdated> ();
						pendingRequests [uri] = slot;

						slot.Add (notify);
					}
				}
			}

			if (uri.IsFile)
				return null;

			QueueRequest (uri, notify);
			return null;
		}

		public static void DeQueueRequest(Uri uri, IImageUpdated notify)
		{
			try{
				if (pendingRequests.ContainsKey (uri)) {
					lock(pendingRequests)
					{
						if (pendingRequests[uri].Count > 1)
							pendingRequests[uri].Remove(notify);
						else 
							pendingRequests.Remove(uri);
					}

					LogHelper.LogDebug("Removed callback from pending queue for {0}", uri.AbsoluteUri);
				}
			} catch{
			}
		}

		public static void DeQueueDownload(Uri uri, IImageUpdated notify = null)
		{

			try{
				DeQueueRequest(uri, notify);
				if (!pendingRequests.ContainsKey (uri))	{
					if(requestQueue.Contains(uri)) {
						lock(requestQueue) {
							requestQueue.Remove(uri);
						}
					}
				}

				//
				//				lock(pendingRequests)
				//				{
				//					if (pendingRequests.ContainsKey (uri))
				//					{
				//						if (pendingRequests[uri].Count > 1 && notify != null) {
				//							DeQueueRequest(uri, notify);
				//							LogHelper.LogDebug("Removed one of multiple callbacks from pendingRequests for {0}", uri.AbsoluteUri);
				//							return;
				//						} else {
				//							pendingRequests.Remove(uri);
				//
				//							lock(requestQueue)
				//							{
				//								if(requestQueue.Contains(uri))
				//								{
				//									requestQueue.Remove(uri);
				//
				//									LogHelper.LogDebug("Removed download from request queue {0}", uri.AbsoluteUri);
				//								}
				//								LogHelper.LogDebug("Removed download from pendingRequests for {0}", uri.AbsoluteUri);
				//							}
				//						}
				//					}
				//				}
			} catch{

			}

		}

		static void QueueRequest (Uri uri, IImageUpdated notify)
		{
			//if (notify == null)
			//throw new ArgumentNullException ("notify");
			//			lock (pendingRequests) {
			//				if (notify != null) {
			//					var slot = new List<IImageUpdated> ();
			//					pendingRequests [uri] = slot;
			//
			//					slot.Add (notify);
			//				}
			//			}

			//Here we start or queue up a download if this uri isnt being downloaded right now already
			if (!currentDownloads.Contains (uri)) {

				if (picDownloaders >= ((notify == null) ? MaxBackgroundRequests : MaxRequests)) {
					//TO DO - if already in the queue we may need to bump it
					//More important if deQueueType is set to Request as all downloads remain in queue
					//in deQueue Download mode downloads are removed and priority is asserted that way


					lock (requestQueue) {
						if (notify != null) {
							if (!requestQueue.Contains (uri)) {
								requestQueue.Add (uri);
							} else {
								requestQueue.Remove (uri);
								requestQueue.Add (uri);
							}
						} else {
							if (!requestQueue.Contains (uri)) {
								requestQueue.Insert (0, uri);
							}
						}
					}

				} else {	
					//Console.Error.WriteLine ("Starting download {0}", uri.ToString());

					//moved increment outside of start download otherwise rapid chacing creates loads of threads before count is incremented
					Interlocked.Increment (ref picDownloaders);

					ThreadPool.QueueUserWorkItem (delegate { 
						try {  
							StartPicDownload (uri); 
						} catch (Exception e) {
							Console.WriteLine (e);
						}
					});
				}
			}

		}

		static void Download (Uri uri, string target)
		{
			string tmpfile = target + ".tmp";
			try {
				currentDownloads.Add(uri);

				if (File.Exists (tmpfile) && !currentDownloads.Contains(uri)) {
					File.Delete (tmpfile);
				}

				// use Java.Net.URL instead of WebClient...

				if (!File.Exists (tmpfile) && !File.Exists (target)) {
					using (WebClient client = new WebClient()) {
						LogHelper.LogDebug("webclient downloading " + uri.AbsoluteUri);
						client.DownloadFile(uri, tmpfile);
					}
					/*
					var imageUrl = new Java.Net.URL(uri.AbsoluteUri);
					var stream = imageUrl.OpenStream();
					//	LogDebug("====== open " + ImageName(uri.AbsoluteUri));
					using (var o = File.Open(tmpfile, FileMode.OpenOrCreate)) {
						byte[] buf = new byte[1024];
						int r;
						while ((r = stream.Read(buf, 0, buf.Length)) > 0) {
							o.Write(buf, 0, r);
						}
					}*/

					File.Move(tmpfile, target);

				}

			}  catch (Exception) {
				if (File.Exists(tmpfile))
				{
					LogDebug(String.Format("Download failed and tmpfile deleted " + uri.AbsoluteUri));
					File.Delete (tmpfile);
				}
				lock (pendingRequests) {
					pendingRequests.Remove (uri);
				}
			} finally {
				lock (currentDownloads) {
					currentDownloads.Remove (uri);
				}
			}
		}

		static long picDownloaders;

		static void StartPicDownload (Uri uri)
		{

			try {
				_StartPicDownload (uri);
			}  catch (Exception){
				Console.Error.WriteLine ("Download failed and all callbacks removed for ", uri.AbsoluteUri);
				pendingRequests.Remove (uri);
			}

			//	Console.Error.WriteLine ("Leaving StartPicDownload {0}", picDownloaders);
			Interlocked.Decrement (ref picDownloaders);
		}

		static void _StartPicDownload (Uri uri)
		{
			lock (requestQueue){
				requestQueue.Remove(uri);
			}
			do {
				string picfile = PicDir + md5 (uri.AbsoluteUri);
				Download (uri, picfile);

				if (File.Exists (picfile)){
					NotifyImageListeners(uri);
				} else if (File.Exists(picfile + ".tmp")) {
					File.Delete (picfile + ".tmp");
				}

				if (requestQueue.Count > 0){

					lock (requestQueue){
						uri = requestQueue.LastOrDefault();
						requestQueue.Remove(uri);
					}

				}  else {
					//Util.Log ("Leaving because requestQueue.Count = {0} NOTE: {1}", requestQueue.Count, pendingRequests.Count);
					uri = null;
				}


			} while (uri != null);
		}

		// Runs on the main thread
		static void NotifyImageListeners (Uri uri)
		{

			if(pendingRequests.ContainsKey(uri)) {
				//	var list = pendingRequests [uri];

				lock (pendingRequests){

					foreach (var pr in pendingRequests [uri]) {
						try {
							pr.UpdatedImage (uri);
						}  catch (Exception e){
							Console.WriteLine (e);
						}
					}
					pendingRequests.Remove (uri);
				}
			}
		}


		public static void DeleteCachedImages(IEnumerable<string> storeNames)
		{

			int deletionCount = 0;

			foreach(string storeName in storeNames)
			{
				try{
					string target = PicDir + storeName;

					if(File.Exists(target))
					{
						File.Delete(target);
						deletionCount++;
					}
				}
				catch{
					//AroundMe.AndroidHelpers.LogInfo ("ImageLoader: failed to remove file form cache", storeName);
				}
			}

			//AroundMe.AndroidHelpers.LogDebug ("ImageLoader", string.Format("Deleted {0} of {1} marked for deletion", deletionCount ,storeNames.Count()));
		}

		/*		
		Use this to help with ADB watching in CMD 
			"c:\Program Files (x86)\Android\android-sdk\platform-tools\adb" logcat -s MonoDroid:* mono:* MWC:* ActivityManager:*
				*/
		public static void LogDebug(string message)
		{
			Console.WriteLine(message);
			Log.Debug("MWC", message);
		}
	}
}

