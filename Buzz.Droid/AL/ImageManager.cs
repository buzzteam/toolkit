using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

using Android.Graphics.Drawables;

using MonoTouch.Dialog.Utilities;
using Buzz.Core.Contracts;
using Buzz.Core.Utils;
using Android.Graphics;

namespace Buzz.Droid.AL
{

	public class ImageManager : IImageCacher
	{
		private static ImageManager instance = new ImageManager();

		public static ImageManager Instance
		{
			get
			{
				if (instance == null)
					instance = new ImageManager ();

				return instance;
			}	
		}

		public IEnumerable<string> CachedImageNames{
			get{
				using (Java.IO.File dir = new Java.IO.File (ImageLoader.PicDir)) {
					if (dir.IsDirectory) {
						return dir.List ();
					} else {
						return new List<string> ();
					}

				}
			}
		}

		public string CachedName(string input)
		{
			return ImageLoader.md5(input);
		}

		public void CacheImage(string url)
		{
			this.GetImage(url, null);
		}

		// this is the actual entrypoint you call
		public Bitmap GetImage(string imageUrl, IImageUpdated notify, int width = 0, int height = 0, bool lowQuality = false)
		{
			//return imageStore.RequestImage (imageUrl, imageUrl, notify);

			Uri uri;

			if(Uri.TryCreate(imageUrl, UriKind.Absolute, out uri))
			{
				LogHelper.LogDebug ("getting {0}", uri.AbsoluteUri);
				return ImageLoader.DefaultRequestImage(uri, notify, width, height, lowQuality);
			}
			/*
			else if (Uri.TryCreate(LocationsHelpers.FormatImageDirectUrlString(imageUrl), UriKind.Absolute, out uri))
			{
				LogHelper.LogDebug ("getting {0}", uri.AbsoluteUri);
				return ImageLoader.DefaultRequestImage(uri, notify);

			}*/
			else
			{
				LogHelper.LogDebug ("Bad URI: {0}", imageUrl);
				return null;
			}

		}

		public void DeQueueRequest(string imageUrl, IImageUpdated notify)
		{
			Uri uri;

			if (Uri.TryCreate (imageUrl, UriKind.Absolute, out uri)) {
				MonoTouch.Dialog.Utilities.ImageLoader.DeQueueRequest (uri, notify);
			}
		}

		public void DeQueueDownload(string imageUrl, IImageUpdated notify = null)
		{
			Uri uri;

			if (Uri.TryCreate (imageUrl, UriKind.Absolute, out uri)) {
				MonoTouch.Dialog.Utilities.ImageLoader.DeQueueDownload (uri);
			}
		}

		public void DeleteFilesFromCache(IEnumerable<string> storeNames)
		{

			MonoTouch.Dialog.Utilities.ImageLoader.DeleteCachedImages (storeNames);
		}


	}
}

