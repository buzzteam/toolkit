using System;
using Android.Preferences;
using Android.Content;
using Buzz.Core.Contracts;

namespace Buzz.Droid.AL
{

	public class PreferencesService : IPreferencesService
		{
			private ISharedPreferences _preferences;
			
		public PreferencesService(Context context){
				_preferences = PreferenceManager.GetDefaultSharedPreferences(context);
			}
			
			public string GetSettingString(string key)
			{
				return _preferences.GetString(key, string.Empty);
			}
			
			public DateTime GetSettingDate(string key)
			{
				DateTime ret;
				
				if(DateTime.TryParse(_preferences.GetString(key, string.Empty), out ret))
				{
					return ret;
				}
				else
				{
					return DateTime.MinValue;
				}
			}
			
			public void SetSettingString(string key, string value)
			{
				ISharedPreferencesEditor pe = _preferences.Edit();
				pe.PutString(key, value);
				pe.Commit();
				pe.Dispose();
			}
			
			public void SetSettingDate(string key, DateTime value)
			{
				SetSettingString(key, value.ToString());
			}
		}

}

