using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;
using Buzz.Core.Contracts;
using Android.Content.PM;


namespace Buzz.Droid.AL
{
	public class SystemService : ISystemService
	{
		private Context context { get; set;}

		public SystemService (Context parentApp)
		{
			context = parentApp;
		}

		#region ISystemService implementation

		public decimal GetPixelDensity()
		{
			var windowService = Application.Context.GetSystemService(Android.Content.Context.WindowService);
			IWindowManager wm = windowService.JavaCast<Android.Views.IWindowManager>();

			DisplayMetrics dm = new DisplayMetrics();
			wm.DefaultDisplay.GetMetrics(dm);
			return (int)(dm.Density);	
		}

		public object GetApplicationContext ()
		{
			return context;
		}

		public bool CanLaunchApp(string app) {

			PackageManager pm = context.PackageManager;
			try {
				pm.GetPackageInfo(app, PackageInfoFlags.Activities);
				return true;
			} catch (PackageManager.NameNotFoundException) {
				return false;
			}
		}
		#endregion
	}
}

