using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Buzz.Core.Utils;

namespace MonoTouch.Dialog.Utilities
{
	public class ImageLoaderStack : Dictionary<Uri,int>
	{

		public Uri Pop()
		{
			LogHelper.LogDebug ("Image Stack", "h {0}, l {1}", this.Where(i=>i.Value > 0).Count(), this.Where(i => i.Value == 0).Count());

			if (this.Count > 0)
			{
				KeyValuePair<Uri,int> temp = this.LastOrDefault(i => i.Value > 0);

				if(temp.Key == null)
				{
					temp = this.LastOrDefault();
				}

				this.Remove(temp.Key);

				return temp.Key;
			}
			else
				return default(Uri);
		}

		public int HighPriorityCount()
		{
			return  this.Where(i => i.Value > 0).Count();
		}

	}
}