﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Buzz.Core.Contracts;
using Buzz.Droid.AL;

namespace Buzz.Droid
{
	[Application]
	public class ToolkitApplication : Application
	{
		public ToolkitApplication(IntPtr handle, global::Android.Runtime.JniHandleOwnership transfer)
			: base(handle, transfer)
		{
		}

		public override void OnCreate()
		{
			base.OnCreate();

			ServiceContainer.Register<ISystemService> (() => new SystemService (Application.Context));
			ServiceContainer.Register<IReachabilityService> (() => new ReachabilityService ());

		}
	}


}
