﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Buzz.Core.Contracts;

namespace Buzz.Droid
{
	[Activity (Label = "ReachabilityActivity")]			
	public class ReachabilityActivity : Activity
	{
		private Button _button;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.ActivityReachability);
			_button = FindViewById<Button> (Resource.Id.reachability_button);

			_button.Click += (sender, e) => {
				IReachabilityService reachability = ServiceContainer.Resolve<IReachabilityService>();

				//	NetworkStatus status = reachability.LocalWifiConnectionStatus();
				NetworkStatus status = reachability.InternetConnectionStatus();

				_button.Text = status.ToString();
			};
		}
	}
}

