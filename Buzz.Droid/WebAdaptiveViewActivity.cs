﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Buzz.Droid.CustomViews;
using Android.Webkit;
using System.Diagnostics;
using System.Threading;
using Android.Util;

namespace Buzz.Droid
{
    [Activity (Label = "WebAdaptiveViewActivity")]			
    public class WebAdaptiveViewActivity : Activity
    {
        private WebAdaptiveView _webAdaptiveView;

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            string data = string.Format (Constants.WebFormat, Constants.WebCss, Constants.TestURL);

            SetContentView (Resource.Layout.AdaptiveWebviewTest);
            _webAdaptiveView = FindViewById<WebAdaptiveView> (Resource.Id.webView);

            _webAdaptiveView.LoadDataWithBaseURL ("file:///android_asset/", data, "text/html", "utf-8", null);

        }

        protected override void OnPause ()
        {
            base.OnPause ();
        }

        protected override void OnResume ()
        {
            base.OnResume ();
        }

    }
}

