﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ZXing.QrCode;
using ZXing;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace Buzz.Droid
{
	[Activity (Label = "QRActivity")]			
	public class QRActivity : Activity
	{
		private EditText _editText;
		private ImageView _imageView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.ActivityQR);

			Button scanButton = FindViewById<Button> (Resource.Id.scanButton);
			Button generateButton = FindViewById<Button> (Resource.Id.generateButton);
			_editText = FindViewById<EditText> (Resource.Id.editText);
			_imageView = FindViewById<ImageView> (Resource.Id.imageView);

			scanButton.Click += async delegate {
				var scanner = new ZXing.Mobile.MobileBarcodeScanner(this);
				var result = await scanner.Scan();

				if (result != null) {
					Console.WriteLine("Scanned Barcode: " + result.Text);
					_editText.Text = result.Text;
				}
			};


			generateButton.Click += delegate {
				Bitmap img = QRHelper.GenerateScaledQR(_editText.Text, 300, 300);

				_imageView.SetImageBitmap(img);
			};
		}
	}
}

