using System;
using Buzz.Core.Constants;

namespace Buzz.Droid
{
	public class Constants : CoreConstants
	{
		public Constants ()
		{
		}

        public const string TestURL = @"<div class='entry-description'><p style=""text-align: center;"">View Our <a href=""http://www.visitsealife.com/gweek/default/sanctuary-vision/"" target=""_blank"">VIDEO</a>&nbsp;Page</p>
 
 <p style=""text-align: center;"">The Seal Sanctuary are a Rescue, Rehabilitation, and Release centre for seals, and most seasons they rescue over 40 pups.</p>
 
 <p style=""text-align: justify;"">The Sanctuary started in the winter of 1958 when a baby seal, only a few hours old, was washed up on the beach at St Agnes. Ken Jones lived with his wife just one hundred yards from the beach, he picked up the pup and took it back to his small garden.&nbsp;For many years he ran a rescue centre for seals and oiled birds, with just one pool, at St Agnes. The news of his work with seals spread, and he received more and more calls about injured seals.</p>
 
 <p style=""text-align: justify;"">The sanctuary has rescued many seals over the years, and most are well enough to be released back into the wild after treatment, but some seals, for various reasons, would not survive back in the wild, so they have them as guests.</p>
 
 <p style=""text-align: center;"">&nbsp;</p>
 </div><div class='entry-times'><h2>Opening Times</h2><table border=""1"" class=""entry-times-table"" style=""width: 100%;"">
    <thead>
        <tr>
            <th scope=""col"">&nbsp;</th>
            <th scope=""col"">Open</th>
            <th scope=""col"">Last Admission</th>
            <th scope=""col"">Closed</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>November - March</td>
            <td>10.00am</td>
            <td>4.00pm</td>
            <td>5.00pm</td>
        </tr>
        <tr style=""display: table-row;"">
            <td>March - November</td>
            <td>10.00am</td>
            <td>3.30pm</td>
            <td>4.30pm</td>
        </tr>
    </tbody>
 </table>
 
 <p>Open everyday apart from Christmas Day</p>
 </div><div class='entry-prices'><h2>Prices</h2><table border=""1"" class=""entry-prices-table"" style=""width: 100%;"">
    <tbody>
        <tr>
            <td>Adult</td>
            <td>&pound;14.40</td>
        </tr>
        <tr>
            <td>Child 3-14</td>
            <td>&pound;12.00</td>
        </tr>
        <tr>
            <td>Concessions 60+</td>
            <td>&pound;13.20</td>
        </tr>
        <tr>
            <td>Student with ID</td>
            <td>&pound;13.20</td>
        </tr>
    </tbody>
 </table>
 
 <p>Don&#39;t miss out on great savings on our website.</p>
 </div><div class='entry-contact'><h2>Contact Details</h2><p>Gweek<br>near Helston<br>TR12 6UG<br><br></p><p><label>Tel:</label>01326 221361<br><label>Email:</label>slcgweek@attractions.com<br></p></div><h2 class='accordian'>Facilities</h2><div><li>Disabled Access</li><li>Dogs Welcome</li><li>WC</li></div><h2  class = 'accordian'>Special Offers</h2><div><p><strong><span style=""line-height: 1.6em;"">PARENT AND TODDLER</span></strong></p>
 
 <p>Book <a href=""https://secure.visitsealife.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&amp;storeId=10672&amp;catalogId=20551&amp;categoryId=23601&amp;productId=262287"">ONLINE</a></p>
 
 <p>Special offer only available outside of school holidays (Term-Time Only).</p>
 
 <p>Includes admission to Cornish Seal Sanctuary.</p>
 
 <p>Offer valid from Monday to Friday.<br />
 Ticket admits one adult and one child 5 years or younger.<br />
 Children under 3 years can visit the attraction for free.<br />
 You must have a child of 5 or under in your party to book this ticket option.</p>
 
 <p>&nbsp;</p>
 
 <p><strong style=""line-height: 1.6em;"">AUTUMN SENIOR SPECIAL</strong></p>
 
 <p><span style=""line-height: 1.6em;"">Book </span><a href=""https://secure.visitsealife.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&amp;storeId=10672&amp;catalogId=20551&amp;categoryId=23601&amp;productId=262305"" style=""line-height: 1.6em;"">ONLINE</a></p>
 
 <p><span style=""color: rgb(51, 51, 51); font-family: sans-serif, Arial, Verdana, 'Trebuchet MS'; font-size: 13px; line-height: 20.796875px; background-color: rgb(255, 255, 255);"">Save Over 50% on General Admission Price</span></p>
 
 <p>See Europe&rsquo;s Busiest Seal Rescue centre in Action<br />
 Fantastic feeds &amp; demonstrations all day (incl. seals, sea lions, otters and penguins)<br />
 Stunning Views in over 40 Acre of Riverside countryside<br />
 *New for 2013* - We&#39;re celebrating 55 years of Seal Rescue with our New &ldquo;Behind the Scenes Experience&rdquo; in the Seal Hospital!<br />
 Relax in our Sanctuary Caf&eacute;<br />
 Free car parking<br />
 PAY ONCE AND COME BACK ALL WEEK!</p>
 
 <p>&nbsp;</p>
 
 <p><strong style=""line-height: 1.6em;"">FAMILY TICKET</strong></p>
 
 <p>Book <a href=""https://secure.visitsealife.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&amp;storeId=10672&amp;catalogId=20551&amp;categoryId=23601&amp;productId=97601"">ONLINE</a></p>
 
 <p>Save up to &pound;9!</p>
 
 <p>Fantastic feeds &amp; demonstrations all day (incl. seals, sea lions, otters and penguins)<br />
 The amazing Rockpool Experience &ndash; Hold a crab or starfish!<br />
 Free car parking<br />
 *New for 2013* - We&#39;re celebrating 55 years of Seal Rescue with our New &ldquo;Behind the Scenes Experience&rdquo; in the Seal Hospital opening at Easter 2013!<br />
 Children&rsquo;s Pirate ship play area &lsquo;Pirate Bay&rsquo;<br />
 PAY ONCE AND COME BACK ALL WEEK!<br />
 Prices from &pound;29.16 excluding VAT<br />
 plus VAT &pound;5.84, total &pound;35.00</p>
 
 <p>&nbsp;</p>
 
 <p><strong style=""line-height: 1.6em;"">GENERAL ADMISSION - ONLINE SAVER</strong></p>
 
 <p>Book <a href=""https://secure.visitsealife.com/webapp/wcs/stores/servlet/ProductDisplay?langId=-1&amp;storeId=10672&amp;catalogId=20551&amp;categoryId=23601&amp;productId=2011000000"">ONLINE</a></p>
 
 <p>Save up to 30%!</p>
 
 <p>Fantastic feeds &amp; demonstrations all day (incl. seals, sea lions, otters and penguins)<br />
 The amazing Rockpool Experience &ndash; Hold a crab or starfish!<br />
 Free car parking<br />
 *New for 2013* - We&#39;re celebrating 55 years of Seal Rescue with our New &ldquo;Behind the Scenes Experience&rdquo; in the Seal Hospital opening at Easter 2013!<br />
 Children&rsquo;s Pirate ship play area &lsquo;Pirate Bay&rsquo;<br />
 PAY ONCE AND COME BACK ALL WEEK!<br />
 Prices from &pound;8.40 excluding VAT<br />
 plus VAT &pound;1.68, total &pound;10.08</p>
 
 <p>&nbsp;</p>
 
 <p>&nbsp;</p>
 </div><h2>Social Media</h2><div style=""height: 50px""><a href=fb://profile/272403596183754><img src=""Web/facebook.png"" class=""social-link""></a><a href=twitter://user?screen_name=Seal_Sanctuary><img src=""Web/twitter.png"" class=""social-link""></a><a href=http://www.youtube.com/user/Sealsanctuary?feature=watch><img src=""Web/youtube.png"" class=""social-link""></a></div>";




public const string WebFormat = "<html><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0\">{0}</head><body><div id=\"heightWrapper\">{1}</div><script src=\"Web/jquery-2.0.2.min.js\"></script><script src=\"Web/appforcornwall.js\"></script></body></html>";






public const string WebCss = @"<style>
                                        body {-webkit-text-size-adjust: none;padding: 0; margin: 10px 10px; font-size: 14px; font-family: arial;}
                                        h1, h2, h3, p {display: block; padding: 0; margin: 0 0 0.5em 0;}
                                        h1 { font-size: 1.5em; font-weight: normal; margin: 0 -20px 0.5em 4px; padding: 0 0 0 20px;}
                                        h2 { padding: 8px 10px;background-color: #FAAC18; margin: 15px -10px 10px; -10px; font-size: 1.25em; font-weight: normal; }
                                        table {border-collapse:separate;}
                                        td {font-size: 0.75em;}
                                        h3 {font-size: 1em; font-weight: bold;} 
                                        td { vertical-align: top; border-bottom: 2px solid #eee;}
                                        ul {padding: 0 0 0 15px; margin: 0px;}
                                        td ul{list-style: none;}
                                        a {font-weight: bold; color:#fbad18;word-break:break-all;}
                                        p label {display: inline-block; min-width: 40px; padding-right: 5px; }
                                        .toggle{ font-family: georgia; display: block; float:right;color: #FAAC18; background-color: #fff ; font-size: 1.25em; line-height: 0.8em; width: 1em; height : 1em; border-radius: 0.5em; text-align: center; font-weight: bold; }
                                        .entry-description p {text-align: justify;}
.social-link {display:block; float:left; padding:5px 5px 15px 5px;width: 50px; height:50px}
.table-wrapper.entry-prices-table{ max-width: 300px;}
table.entry-prices-table{ border-width: 0  !important;border-spacing:0;border-collapse:collapse;  margin-bottom: 20px;}
table.entry-prices-table td, table.entry-prices-table th{ border: 1px solid #cccccc; border-width: 1px 0 !important;border-spacing:0;border-collapse:collapse;  height: 17px; font-size: 12px; text-align: center; font-weight: normal;}
table.entry-prices-table td:first-child, table.entry-prices-table th:first-child{ text-align: left; }
table.entry-prices-table td:last-child, table.entry-prices-table th:last-child{ text-align: right; }
table.entry-prices-table th{ font-weight: bold; text-align: left;}
table.entry-prices-table p{margin: 0;}
span.price {display:inline-block; min-width: 40px;}

.entry-times .table-wrapper{ max-width: 300px;}
table.entry-times-table{border-style: none !important;   border-width: 0  !important;border-spacing:0;border-collapse:collapse;  margin-bottom: 20px;}
table.entry-times-table td,table.entry-times-table th{ border: 1px solid #cccccc; border-width: 1px 0 !important;border-spacing:0;border-collapse:collapse;  height: 17px; font-size: 12px;text-align: center; font-weight: normal;}
table.entry-times-table th{ font-weight: bold; }
table.entry-times-table p{margin: 0;}

table.entry-times-table td:first-child , table.entry-times-table th:first-child{text-align: left; }

.accordian+div {display: none;}
</style>";


		public const string PageTitleAboutMe = "About Me";
		public static string LocationDisabledAlert = "You allowed location sharing when you configured Meniere's Monitor, however location sharing is disabled in your phone's Settings. Please enable location sharing (Settings - Location Access) to help us make the most of your valued participation.";
	}
}

