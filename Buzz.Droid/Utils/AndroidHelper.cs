using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Net;
using Android.Service;

using Android.Locations;
using Android.Util;
using Android.Graphics;
using Android.Graphics.Drawables;

namespace Buzz.Droid.Utils
{
	public enum ScreenOrientation
	{
		Portrait,
		Landscape,
		Unditermined
	}
	
	class AndroidHelper
	{

		public static int GetPixels(int dp)
		{
			var windowService = Application.Context.GetSystemService(Android.Content.Context.WindowService);
			IWindowManager wm = windowService.JavaCast<Android.Views.IWindowManager>();
			
			DisplayMetrics dm = new DisplayMetrics();
			wm.DefaultDisplay.GetMetrics(dm);
			float density = dm.Density;
			return (int)(dp * density + 0.5f);
		}
		
		public static int GetDensity()
		{
			var windowService = Application.Context.GetSystemService(Android.Content.Context.WindowService);
			IWindowManager wm = windowService.JavaCast<Android.Views.IWindowManager>();
			
			DisplayMetrics dm = new DisplayMetrics();
			wm.DefaultDisplay.GetMetrics(dm);
			return (int)(dm.Density);	
		}
		
		public static int GetScreenWidth()
		{
			
			var windowService = Application.Context.GetSystemService(Android.Content.Context.WindowService);
			IWindowManager wm = windowService.JavaCast<Android.Views.IWindowManager>();
			
			DisplayMetrics dm = new DisplayMetrics();
			wm.DefaultDisplay.GetMetrics(dm);
			
			return dm.WidthPixels;
		}
		
		public static int GetScreenHeight()
		{
			var windowService = Application.Context.GetSystemService(Android.Content.Context.WindowService);
			IWindowManager wm = windowService.JavaCast<Android.Views.IWindowManager>();
			
			DisplayMetrics dm = new DisplayMetrics();
			wm.DefaultDisplay.GetMetrics(dm);
			
			return dm.HeightPixels;
		}
		
		
        public static bool ResizeBitmapDrawable(int newWidthDp, int newHeightDp, ref BitmapDrawable bd) 
		{   
			
			
			float scaleWidth = ((float) AndroidHelper.GetPixels(newWidthDp)) /(float) bd.IntrinsicWidth ;
	        float scaleHeight =((float) AndroidHelper.GetPixels(newHeightDp)) /(float) bd.IntrinsicHeight;
			
			float scale;
			
			if(scaleWidth <= scaleHeight)
				scale = scaleWidth;
			else
				scale = scaleHeight;

			Bitmap b = Bitmap.CreateScaledBitmap(bd.Bitmap, (int)(bd.IntrinsicWidth * scale), (int)(bd.IntrinsicHeight * scale), false);
			
			bd = new BitmapDrawable(Application.Context.Resources, b);
			//bd.SetBounds(0,0,b.Width, b.Height);
			
			return true;
			/*
			float scaleWidth = ((float) GetPixels(newWidthDp)) / d ;
	        float scaleHeight = ((float) GetPixels(newHeightDp)) / bitmapOrig.Height;
			
			
			
			
			
			Bitmap bitmapOrig = bdImage.Bitmap;
	        
			float scale;
			if()
			
	        Matrix matrix = new Matrix();
	        matrix.PostScale(scaleWidth, scaleHeight);
	        Bitmap resizedBitmap = Bitmap.CreateBitmap(bitmapOrig, 0, 0, bitmapOrig.Width, bitmapOrig.Height, matrix, true);
	        BitmapDrawable bitmapDrawableResized = new BitmapDrawable(resizedBitmap);
	        return bitmapDrawableResized; */
        }
		
		public static int CalculateInSampleSize(BitmapFactory.Options options, 
                                        int reqWidth, int reqHeight)
		{
		    // Raw height and width of image
		    var height = (float)options.OutHeight;
		    var width = (float)options.OutWidth;
			int inSampleSize = 1;
		
		    if (height > reqHeight || width > reqWidth)
		    {
				float halfHeight = height / 2;
				float halfWidth = width / 2;

				while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
		    }
		
		    return (int) inSampleSize;
		}

	
		public static double CalculateInSampleSize(int width, int height, 
		                                        int reqWidth, int reqHeight)
		{
			if (width == 0 || height == 0 || reqWidth == 0 || reqHeight == 0)
				return 1;
		
			var inSampleSize = 1D;

			if (height > reqHeight || width > reqWidth)
			{
				inSampleSize = width > height
					? (double)height/(double)reqHeight
						: (double)width/(double)reqWidth;
			}

			return inSampleSize;
		}



		public static Bitmap SampledBitmapFromDrawable(Drawable drawable, int reqWidth, int reqHeight, bool lowQuality = false)
		{
			double sampleSize = CalculateInSampleSize (drawable.IntrinsicWidth, drawable.IntrinsicHeight, reqWidth, reqHeight);

			if(lowQuality)
				sampleSize = sampleSize * 2;

				
			Bitmap bitmap = Bitmap.CreateBitmap((int)((double)drawable.IntrinsicWidth/sampleSize), (int)((double)drawable.IntrinsicHeight/sampleSize), Bitmap.Config.Argb8888);
				Canvas canvas = new Canvas(bitmap);
			drawable.SetBounds(0, 0,  (int)((double)drawable.IntrinsicWidth/sampleSize)  , (int)((double)drawable.IntrinsicHeight/sampleSize));
			 	drawable.Draw(canvas);

				return bitmap;
		}

		public static Bitmap DecodeSampledBitmapFromBytes(byte[] data, 
                                                     int reqWidth, int reqHeight, bool lowQuality = false)
		{
		    // First decode with inJustDecodeBounds=true to check dimensions
		    using (var options = new BitmapFactory.Options { InJustDecodeBounds = true })
		    {
				BitmapFactory.DecodeByteArray(data, 0, data.Length, options);
		       // BitmapFactory.DecodeResource(res, resId, options); 
		
		        // Calculate inSampleSize
		        options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);
		
				if(lowQuality)
					options.InSampleSize = options.InSampleSize * 2;

		        // Decode bitmap with inSampleSize set
		        options.InJustDecodeBounds = false;

		        return BitmapFactory.DecodeByteArray(data, 0, data.Length, options);
		    }
		}

		public static Bitmap DecodeSampledBitmapFromPath(string path, 
			int reqWidth, int reqHeight, bool lowQuality = false)
		{
			// First decode with inJustDecodeBounds=true to check dimensions
			using (var options = new BitmapFactory.Options { InJustDecodeBounds = true })
			{
				BitmapFactory.DecodeFile (path, options);
				// BitmapFactory.DecodeResource(res, resId, options); 

				// Calculate inSampleSize
				options.InSampleSize = CalculateInSampleSize(options, reqWidth, reqHeight);

				if(lowQuality)
					options.InSampleSize = options.InSampleSize * 2;

				// Decode bitmap with inSampleSize set
				options.InJustDecodeBounds = false;

				return BitmapFactory.DecodeFile (path, options);
			}
		}
	}
}

