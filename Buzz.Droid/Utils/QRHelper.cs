﻿using System;
using Android.Graphics;
using ZXing.QrCode;
using ZXing;
using Buzz.Droid.Utils;

namespace Buzz.Droid
{
	public class QRHelper
	{
		public QRHelper ()
		{
		}

		public static Bitmap GenerateQR(string data)
		{
			var size = 0;
			var writer = new QRCodeWriter(); 
			var matrix = writer.encode(data, BarcodeFormat.QR_CODE, size, size); 

			Bitmap image = Bitmap.CreateBitmap(matrix.Width, matrix.Height, Bitmap.Config.Argb8888); 

			for (int y = 0; y < matrix.Height; ++y) { 
				for (int x = 0; x < matrix.Width; ++x) { 
					if (matrix[x, y]) {
						image.SetPixel(x, y, Color.Black);
					} else {
						image.SetPixel(x, y, Color.White);
					} 
				} 
			}
			return image;
		}

		public static Bitmap GenerateScaledQR(string data, int width, int height)
		{
			Bitmap image = GenerateQR (data);

			return Bitmap.CreateScaledBitmap (image, AndroidHelper.GetPixels (width), AndroidHelper.GetPixels (height), false);
		}
	}
}

