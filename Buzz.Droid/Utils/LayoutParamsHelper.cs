using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Buzz.Droid.Utils
{
	public class LayoutParamsHelper
	{
		public static class Relative
		{
			public static RelativeLayout.LayoutParams MatchParentParams {
				get {
					return new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
				}
			}

			public static RelativeLayout.LayoutParams CenterInParentParams {
				get {
					var lp = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
					lp.AddRule (LayoutRules.CenterInParent);
					return lp;
				}
			}

			public static RelativeLayout.LayoutParams CenterBottomInParentParams {
				get {
					var lp = new RelativeLayout.LayoutParams (RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
					lp.AddRule (LayoutRules.CenterHorizontal);
					lp.AddRule (LayoutRules.AlignParentBottom);
					return lp;
				}
			}
		}

	}
}

