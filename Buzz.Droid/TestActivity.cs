using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MonoTouch.Dialog.Utilities;
using Buzz.Core.Utils;
using Buzz.Droid.AL;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace Buzz.Droid
{
	[Activity (Label = "TestActivity")]			
	public class TestActivity : Activity, IImageUpdated
	{
		private ImageView _imageView;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.ActivityManual);
			_imageView = FindViewById<ImageView> (Resource.Id.imageView);

			/*Drawable _drawable = ImageManager.Instance.GetImage (Constants.ImageUrl4, this);

			if (_drawable != null) {
				_imageView.SetImageDrawable (_drawable);
				_drawable.Dispose ();
			}*/

			//	string picFile = "/mnt/sdcard/Android/data/uk.co.buzzinteractive.toolkit/cache/Pictures.MonoTouch.Dialog/12450a54563b80ab5099103ad334fdad";
			//	Bitmap _bitmap = BitmapFactory.DecodeFile (picFile);

			Bitmap _bitmap = ImageManager.Instance.GetImage (Constants.ImageUrl3, this, 400, 400);
			if (_bitmap != null) {
				_imageView.SetImageBitmap (_bitmap);
				_bitmap.Dispose ();
			}


		}

		public void UpdatedImage(Uri uri)
		{
			LogHelper.LogDebug ("Image updated {0}", uri.AbsoluteUri);
			/*	Drawable _drawable = ImageManager.Instance.GetImage (uri.AbsoluteUri, null);
			if (_drawable != null) {

				Bitmap bm = AndroidHelpers.SampledBitmapFromDrawable(_drawable , _imageView.MeasuredWidth, _imageView.MeasuredHeight);

				RunOnUiThread (() => {
					_imageView.SetImageBitmap (bm);
					//	_imageView.SetImageDrawable (_drawable);
					bm.Dispose ();

					((BitmapDrawable)_drawable).Bitmap.Recycle();
					_drawable.Dispose ();
					_drawable = null;
				});
			}*/
			Bitmap bitmap = ImageManager.Instance.GetImage (uri.AbsoluteUri, null);
			if (bitmap != null) {

				RunOnUiThread (() => {
					_imageView.SetImageBitmap (bitmap);
					bitmap.Dispose ();

				});
			}
		}

		protected override void OnDestroy ()
		{
			//	_bitmap.Dispose ();
			_imageView.SetImageBitmap (null);
			_imageView.Dispose ();

			base.OnDestroy ();
		}
	}
}

