if(!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g,'');
  };
}

// Constants
var growTime = 0;//400;
var shrinkTime = 0;//100;
var resizeTime = 200;

// Init accordians
$('.accordian').next().css("display", "block");
$('.accordian').next().hide();
$('.accordian').append('<span class="toggle">+</span>');
 




$('.accordian').click(function()
	{
		// Get handle on clicked accordian
		var visible = $(this).next().is(":visible")

		// Shrink Android Webview
		Android.WillShrink();

		// Close ALL accordians over 'shrinktime' milliseconds
		$('.accordian').next().hide(shrinkTime);
		$('.accordian').children('span').html('+');

		// If clicked accordian wasn't expanded...
		if (!visible) 
		{
			// Change its icon to "-" and expand it over 'growTime' ms, then resize Android Webview to Content height over 'resizeTime' ms
			$(this).children('span').html('-');
			$(this).next().show(growTime, function()
			{
				ResizeDelay(resizeTime);
			});
		}
		// else if it was expanded
		else
		{
			// resize Android Webview after 'shrinkTime + resizeTime ms)
			ResizeDelay(shrinkTime+resizeTime);
		}
	});


$('.entry-prices table,.entry-times table').each(function()
	{
 		$(this).wrap('<div class="table-wrapper">');
	});
 
 

$('.entry-prices table td,.entry-times table td').each(function () {

var text = $(this).html();

if (text.indexOf('£') > -1) 
	{
	    var preText = text.substring(0, text.indexOf('£'));
	    var textToAlter = text.substring(text.indexOf('£') + 1);

		if(textToAlter.trim() != '')
		{
		    var prices = textToAlter.split('£');

		    if (prices.length == 1)
		    {
		        var newText = '&pound;' + '<span class="price">' + prices[0] + '</span>';
		        $(this).html(preText + newText);
		    }
	    }
	}
});

// Interface to Android...Set Webview height to its Content height after 'delay' ms
function ResizeDelay (delay)
	{
		setTimeout(function()
		{
			Android.DidResize();
		},delay);
	}

