using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Buzz.Droid.CustomViews;
using Buzz.Core.Utils;

namespace Buzz.Droid
{
	[Activity (Label = "ImageLoaderActivity")]			
	public class ImageLoaderActivity : Activity
	{
		private ImageLoaderView imageLoader;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.ActivityImageLoader);

			imageLoader = FindViewById<ImageLoaderView> (Resource.Id.ImageLoaderTest);
			TextView textView = FindViewById<TextView> (Resource.Id.textView1);

			//imageLoader.ImageLoaded += delegate {
			//	textView.Text = "event happened text view";
			//};
			//	imageLoader.Url = Constants.ImageUrl2;



			imageLoader.Url = Constants.ImageUrl1;
			//	imageLoader.Url = Constants.ImageUrl3;


		}

		protected override void OnDestroy ()
		{
			imageLoader.Dispose ();

			base.OnDestroy ();
		}
	}
}

