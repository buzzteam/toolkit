 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Buzz.Droid.CustomViews;
using Buzz.Core.Utils;

namespace Buzz.Droid
{
	[Activity (Label = "ImageLoadingIndicatorActivity")]			
	public class ImageLoadingIndicatorActivity : Activity
	{
		private ImageLoadingIndicatorView _imageLoadingIndicator;
		private ImageLoadingIndicatorView _imageLoadingIndicator2;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);


			SetContentView (Resource.Layout.ActivityImageLoadingIndicator);


			_imageLoadingIndicator = FindViewById<ImageLoadingIndicatorView> (Resource.Id.ImageLoadingIndicatorTest);
			_imageLoadingIndicator2 = FindViewById<ImageLoadingIndicatorView> (Resource.Id.ImageLoadingIndicatorTest2);

			_imageLoadingIndicator.ImageLoaded += delegate {
				LogHelper.LogDebug("image loaded activity callback");
			};
		
			_imageLoadingIndicator.Url = Constants.ImageUrl5;


			_imageLoadingIndicator2.Url = Constants.ImageUrl1;
			_imageLoadingIndicator2.Url = Constants.ImageUrl2;
			_imageLoadingIndicator2.Url = Constants.ImageUrl3;

		}

		protected override void OnDestroy ()
		{
			_imageLoadingIndicator.Dispose ();
			_imageLoadingIndicator2.Dispose ();

			base.OnDestroy ();
		}

	}
}

