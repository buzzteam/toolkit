if(!String.prototype.trim) {
  String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g,'');
  };
}

notifyIOS = function(code) {
  var iframe = document.createElement("IFRAME");
  iframe.setAttribute("src", "ios-notify:" + code);
  document.documentElement.appendChild(iframe);
  iframe.parentNode.removeChild(iframe);
  iframe = null;    
}


//$('.accordian').next().hide();
$('.accordian').append('<span class="toggle">+</span>');

$('.accordian').click(function(){

  notifyIOS('ResizeScrollView/0/0');
 
		
		var visible = $(this).next().is(":visible")

		if(visible)
		{
			$(this).children('span').html('+');
		}
		else
		{
			$(this).children('span').html('-');
		}
		
		$(this).next().toggle(0,function(){

			setTimeout(function(){

				var h2 = $(this).prev().first();
				var div = $(this).first();

				var offset = h2.offset();
				var height = div.outerHeight();
				height +=  h2.outerHeight();
				height += 20;
					
			},10);

		});

});



 $('.entry-prices table,.entry-times table').each(function(){
 	$(this).wrap('<div class="table-wrapper">');
 });
 
 

 $('.entry-prices table td,.entry-times table td').each(function () {

    var text = $(this).html();

    if (text.indexOf('£') > -1) {
        var preText = text.substring(0, text.indexOf('£'));
        var textToAlter = text.substring(text.indexOf('£') + 1);

		if(textToAlter.trim() != '')
		{
		
        var prices = textToAlter.split('£');
        

        if (prices.length == 1)
        {
            
            var newText = '&pound;' + '<span class="price">' + prices[0] + '</span>';
             $(this).html(preText + newText);
        }
        }
    }


});

	

