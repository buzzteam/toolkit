// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace Buzz.iOS
{
	[Register ("ImageLoaderViewController")]
	partial class ImageLoaderViewController
	{
		[Outlet]
		Buzz.iOS.CustomViews.ImageLoaderView ImageLoaderTest { get; set; }

		[Outlet]
		Buzz.iOS.CustomViews.ImageLoaderView ImageLoaderTest2 { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ImageLoaderTest != null) {
				ImageLoaderTest.Dispose ();
				ImageLoaderTest = null;
			}

			if (ImageLoaderTest2 != null) {
				ImageLoaderTest2.Dispose ();
				ImageLoaderTest2 = null;
			}
		}
	}
}
