// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace Buzz.iOS
{
	[Register ("WebAdaptiveAndImageSliderVC")]
	partial class WebAdaptiveAndImageSliderVC
	{
		[Outlet]
		Buzz.iOS.CustomViews.ImageSliderView _imageSlider { get; set; }

		[Outlet]
		Buzz.iOS.CustomViews.WebAdaptiveView _webView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (_imageSlider != null) {
				_imageSlider.Dispose ();
				_imageSlider = null;
			}

			if (_webView != null) {
				_webView.Dispose ();
				_webView = null;
			}
		}
	}
}
