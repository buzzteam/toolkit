// This file has been autogenerated from a class added in the UI designer.

using System;

using MonoTouch.Foundation;
using MonoTouch.UIKit;
using ZXing.QrCode;
using ZXing.Common;
using System.Drawing;
using ZXing;
using MonoTouch.CoreGraphics;
using MonoTouch.CoreAnimation;

namespace Buzz.iOS
{
	public partial class QRViewController : UIViewController
	{
		public QRViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			Button.TouchUpInside += async delegate {
				var scanner = new ZXing.Mobile.MobileBarcodeScanner();
				var result = await scanner.Scan();

				if (result != null)
					Console.WriteLine("Scanned Barcode: " + result.Text);
			};

			TextField.ShouldReturn += delegate { 
				TextField.ResignFirstResponder();
				return true; 
			};

			GenerateButton.TouchUpInside += delegate {
				UIImage qr = QRHelper.GenerateQR(TextField.Text);
				QRHelper.DisableAntiAlias(ImageView);

				ImageView.Image = qr;
			};
		}
	}
}
