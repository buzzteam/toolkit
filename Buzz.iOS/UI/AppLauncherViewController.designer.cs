// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace Buzz.iOS
{
	[Register ("AppLauncherViewController")]
	partial class AppLauncherViewController
	{
		[Outlet]
		MonoTouch.UIKit.UIButton Button { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIButton Button2 { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel Label { get; set; }

		[Outlet]
		MonoTouch.UIKit.UITextField TextField { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIWebView WebClipTest { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (Button != null) {
				Button.Dispose ();
				Button = null;
			}

			if (Button2 != null) {
				Button2.Dispose ();
				Button2 = null;
			}

			if (Label != null) {
				Label.Dispose ();
				Label = null;
			}

			if (TextField != null) {
				TextField.Dispose ();
				TextField = null;
			}

			if (WebClipTest != null) {
				WebClipTest.Dispose ();
				WebClipTest = null;
			}
		}
	}
}
