// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;
using System.CodeDom.Compiler;

namespace Buzz.iOS
{
	[Register ("ImageSliderViewController")]
	partial class ImageSliderViewController
	{
		[Outlet]
		Buzz.iOS.CustomViews.ImageSliderView ImageSlider { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (ImageSlider != null) {
				ImageSlider.Dispose ();
				ImageSlider = null;
			}
		}
	}
}
