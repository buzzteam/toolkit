using System;

using MonoTouch.Foundation;

using Buzz.Core.Contracts;
using MonoTouch.UIKit;

namespace Buzz.iOS.AL
{
	public class PreferencesService : IPreferencesService
	{
		private  NSUserDefaults _plist;
		
		public PreferencesService(){
			_plist =  NSUserDefaults.StandardUserDefaults;
		}
		
		public string GetSettingString(string key)
		{
			return _plist.StringForKey(key);
		}
		
		public DateTime GetSettingDate(string key)
		{
			DateTime ret;
			
			if(DateTime.TryParse(_plist.StringForKey(key), out ret))
			{
				return ret;
			}
			else
			{
				return DateTime.MinValue;
			}
		}

		public void SetSettingString(string key, string value)
		{
			_plist.SetString(value, key);
			_plist.Synchronize();
		}
		
		public void SetSettingDate(string key, DateTime value)
		{
			_plist.SetString(value.ToString(), key);
			_plist.Synchronize();
		}
	}
}

