using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;

using MonoTouch.Dialog.Utilities;
using MonoTouch.UIKit;

using Buzz.Core.Contracts;
using Buzz.Core.Utils;

namespace Buzz.iOS.AL
{
	
	public class ImageManager : IImageCacher
	{
		private static ImageManager instance = new ImageManager();
		
		public static ImageManager Instance
		{
			get
			{
				if (instance == null)
					instance = new ImageManager ();
				
				return instance;
			}	
		}

		public IEnumerable<string> CachedImageNames{
			get{
				return System.IO.Directory.EnumerateFiles(ImageLoader.PicDir).Select(Path.GetFileName).ToList();
				//return System.IO.Directory.GetFiles(ImageLoader.PicDir);
			}
		}

		public string CachedName(string input)
		{
			//return ImageLoader.md5 (input);

			Uri uri;

			if (Uri.TryCreate (input, UriKind.Absolute, out uri)) {
				return ImageLoader.md5 (uri.AbsoluteUri);
			} else {
				return string.Empty;
			}
		}

		public void CacheImage(string url)
		{
			this.GetImage(url, null);
		}
		
		// this is the actual entrypoint you call
		public UIImage GetImage(string imageUrl, IImageUpdated notify)
		{
			//return imageStore.RequestImage (imageUrl, imageUrl, notify);
			
			Uri uri;
			if(Uri.TryCreate(imageUrl, UriKind.Absolute, out uri))
			{
				LogHelper.LogDebug ("getting {0}", uri.AbsoluteUri);
				return ImageLoader.DefaultRequestImage(uri, notify);
			}/*
			else if (Uri.TryCreate(LocationsHelpers.FormatImageDirectUrlString(imageUrl), UriKind.Absolute, out uri))
			{
				LogHelper.LogDebug ("getting {0}", uri.AbsoluteUri);
				return ImageLoader.DefaultRequestImage(uri, notify);
			}*/
			else
			{
				LogHelper.LogDebug ("Bad URI: {0}", imageUrl);
				return null;
			}

		}

		public void DeQueueRequest(string imageUrl, IImageUpdated notify)
		{
			Uri uri;

			if (Uri.TryCreate (imageUrl, UriKind.Absolute, out uri)) {
				MonoTouch.Dialog.Utilities.ImageLoader.DeQueueRequest (uri, notify);
			}
		}

		public void DeQueueDownload(string imageUrl, IImageUpdated notify = null)
		{
			Uri uri;

			if (Uri.TryCreate (imageUrl, UriKind.Absolute, out uri)) {
				MonoTouch.Dialog.Utilities.ImageLoader.DeQueueDownload (uri);
			}
		}

		public void DeleteFilesFromCache(IEnumerable<string> storeNames)
		{

			MonoTouch.Dialog.Utilities.ImageLoader.DeleteCachedImages (storeNames);
		}
		
			
	}
}

