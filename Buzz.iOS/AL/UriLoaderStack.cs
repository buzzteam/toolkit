using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace Buzz.iOS
{
	public class UriLoaderStack : Dictionary<Uri,int>
	{
	
		public Uri Pop()
		{
			if (this.Count > 0)
			{
				KeyValuePair<Uri,int> temp = this.LastOrDefault(i => i.Value > 0);

				if(temp.Key == null)
				{
					temp = this.LastOrDefault();
				}

				this.Remove(temp.Key);

				return temp.Key;
			}
			else
				return default(Uri);
		}

		public int HighPriorityCount()
		{
				return  this.Where(i => i.Value > 0).Count();
		}

	}
}

