using System;

using MonoTouch.UIKit;
using MonoTouch.Foundation;
using Buzz.Core.Contracts;

namespace Buzz.iOS.AL
{
	public class 
	SystemService : ISystemService
	{
		public SystemService ()
		{
		}

		#region ISystemService implementation

		public decimal GetPixelDensity ()
		{
			return (Decimal)Math.Ceiling(UIScreen.MainScreen.Scale * 2) / 2;
		}

		#endregion

		public bool CanLaunchApp(string app) {
			return UIApplication.SharedApplication.CanOpenUrl(new NSUrl(app));
		}


	}
}

