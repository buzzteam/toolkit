using System;
using MonoTouch.UIKit;

namespace Buzz.iOS.CustomViews
{
	public class WebAdaptiveViewController : UIViewController
	{
		private UIScrollView _scrollView;
		public event EventHandler WillRotateViewController;
		public WebAdaptiveView WebView;

		public UIScrollView ScrollView {
			get {
				return _scrollView;
			}
			set {
				_scrollView = value;
			}
		}

		public WebAdaptiveViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			WebView.ViewController = this;
		}

		public override void WillRotate (UIInterfaceOrientation toInterfaceOrientation, double duration)
		{
			base.WillRotate (toInterfaceOrientation, duration);

			if (WillRotateViewController != null) {
				WillRotateViewController (this, new EventArgs ());
			}
		}
	}
}

