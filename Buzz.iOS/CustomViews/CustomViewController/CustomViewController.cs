// This file has been autogenerated from a class added in the UI designer.

using System;

using MonoTouch.Foundation;
using MonoTouch.UIKit;

namespace Buzz.iOS.CustomViews
{
	public class CustomViewController : UIViewController
	{
		public delegate void PrepareForSegueDelegate(UIViewController destinationController);
		private PrepareForSegueDelegate _prepareForSegueDelegate;

		public event EventHandler DidDisappear;

		public CustomViewController (IntPtr handle) : base (handle)
		{
		}

		public void PerformSegue (string identifier, NSObject sender, PrepareForSegueDelegate setupDestinationViewController, bool setNavigationBarHidden)
		{
			PerformSegue (identifier, sender, setupDestinationViewController);
			NavigationController.SetNavigationBarHidden(setNavigationBarHidden, true);
		}

		public void PerformSegue (string identifier, NSObject sender, PrepareForSegueDelegate setupDestinationViewController)
		{
			_prepareForSegueDelegate = setupDestinationViewController;
			PerformSegue(identifier, sender);
		}

		public override void PrepareForSegue (UIStoryboardSegue segue, NSObject sender)
		{
			if (_prepareForSegueDelegate != null)
				_prepareForSegueDelegate.Invoke(segue.DestinationViewController);

			_prepareForSegueDelegate -= _prepareForSegueDelegate;
			base.PrepareForSegue (segue, sender);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);

			if (DidDisappear != null)
				DidDisappear (this, new EventArgs ());
		}
	}
}
