using System;
using MonoTouch.UIKit;

namespace Buzz.iOS.Utils
{
	public class ConstraintsHelper
	{
		public ConstraintsHelper ()
		{
		}

		//Match parent constraints
		public static NSLayoutConstraint[] MatchParent(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Left,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Left,
					1, 0),

				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Right,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Right,
					1, 0),

				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Top,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Top,
					1, 0),

				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Bottom,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Bottom,
					1, 0)
			};
		}

		public static NSLayoutConstraint[] MatchParentHeight(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Top,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Top,
					1, 0),

				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Bottom,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Bottom,
					1, 0)
			};
		}

		public static NSLayoutConstraint[] MatchParentWidth(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Left,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Left,
					1, 0),

				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Right,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Right,
					1, 0)
			};
		}

		//Position in parent constraints
		public static NSLayoutConstraint[] CenterInParent(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.CenterX,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.CenterX,
					1, 0),
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.CenterY,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.CenterY,
					1, 0)
			};
		}

		public static NSLayoutConstraint[] CenterBottomInParent(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.CenterX,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.CenterX,
					1, 0),
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Bottom,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Bottom,
					1, 0)
			};
		}

		public static NSLayoutConstraint TopInParent(UIView child, UIView parent)
		{
			return 	NSLayoutConstraint.Create (
				child,
				NSLayoutAttribute.Top,
				NSLayoutRelation.Equal,
				parent,
				NSLayoutAttribute.Top,
				1, 0);
		}

		//Width and height constraints
		public static NSLayoutConstraint[] ExplicitWidthHeight(UIView view, float width, float height)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					view,
					NSLayoutAttribute.Width,
					NSLayoutRelation.Equal,
					null,
					NSLayoutAttribute.NoAttribute,
					1, width),
				NSLayoutConstraint.Create (
					view,
					NSLayoutAttribute.Height,
					NSLayoutRelation.Equal,
					null,
					NSLayoutAttribute.NoAttribute,
					1, height)
			};
		}

		public static NSLayoutConstraint ExplicitHeight(UIView view, float height)
		{
			return NSLayoutConstraint.Create (
				view,
				NSLayoutAttribute.Height,
				NSLayoutRelation.Equal,
				null,
				NSLayoutAttribute.NoAttribute,
				1, height);
		}

		public static NSLayoutConstraint[] EqualToParentWidthHeight(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Width,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Width,
					1, 0),
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Height,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Height,
					1, 0)
			};
		}

		public static NSLayoutConstraint EqualToParentHeight(UIView child, UIView parent)
		{
			return NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Height,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Height,
				1, 0);
		}

		public static NSLayoutConstraint HeightRatio(UIView view, float ratio)
		{
			return NSLayoutConstraint.Create (
					view,
					NSLayoutAttribute.Height,
					NSLayoutRelation.Equal,
					view,
					NSLayoutAttribute.Width,
					ratio, 0);
		}

		//Chained views constraints
		public static NSLayoutConstraint[] HorizontalChain(UIView leadingView, UIView trailingView)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					leadingView,
					NSLayoutAttribute.Leading,
					NSLayoutRelation.Equal,
					trailingView,
					NSLayoutAttribute.Trailing,
					1, 0)
			};
		}

		public static NSLayoutConstraint[] HorizontalChainFirst(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Leading,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Leading,
					1, 0)
			};
		}

		public static NSLayoutConstraint[] HorizontalChainLast(UIView child, UIView parent)
		{
			return new NSLayoutConstraint[] {
				NSLayoutConstraint.Create (
					child,
					NSLayoutAttribute.Trailing,
					NSLayoutRelation.Equal,
					parent,
					NSLayoutAttribute.Trailing,
					1, 0)
			};
		}
	}
}	

