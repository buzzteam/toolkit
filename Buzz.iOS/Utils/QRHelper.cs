﻿using System;
using MonoTouch.UIKit;
using ZXing.Common;
using ZXing.QrCode;
using MonoTouch.CoreGraphics;
using ZXing;
using System.Drawing;
using MonoTouch.CoreAnimation;

namespace Buzz.iOS
{
	public class QRHelper
	{
		public QRHelper ()
		{
		}

		public static UIImage GenerateQR(string data)
		{
			UIImage image;
			QRCodeWriter writer = new QRCodeWriter(); 
			BitMatrix matrix;
			int size = 0;
			matrix = writer.encode(data, BarcodeFormat.QR_CODE, size, size, null);

			SizeF qrcCodeSize = new SizeF(matrix.Width, matrix.Height);

			UIGraphics.BeginImageContext(qrcCodeSize);

			using (CGContext cont = UIGraphics.GetCurrentContext ()) {
				cont.SetLineWidth (1);
				cont.SetFillColor (UIColor.White.CGColor);
				cont.AddRect (new RectangleF (0, 0, qrcCodeSize.Width, qrcCodeSize.Height));
				cont.DrawPath (CGPathDrawingMode.Fill);
				cont.SetFillColor (UIColor.Black.CGColor);

				for (int y = 0; y < matrix.Height; y++) {
					for (int x = 0; x < matrix.Width; x++) {
						if (matrix [x, y])
							cont.AddRect (new RectangleF (x, y, 1, 1));
					}
				}
				cont.DrawPath (CGPathDrawingMode.Fill);

				image = UIGraphics.GetImageFromCurrentImageContext ();
			}
			UIGraphics.EndImageContext();
			return image;
		}

		public static void DisableAntiAlias(UIImageView imageView)
		{
			imageView.Layer.MagnificationFilter = CALayer.FilterNearest;
		}
	}
}

