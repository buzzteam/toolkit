using System;
using System.Collections.Generic;
using System.Linq;


using Newtonsoft.Json;
using Buzz.Core.Contracts;
using Buzz.Core.DAL.SQLite;
using System.Runtime.Serialization;

namespace Buzz.Core.Models
{
	public class Config : IEntity
	{
		[PrimaryKey, AutoIncrement]
		public int Id {get; set;}

		public DateTime UpdatedDate { get; set; }
		/*
		[OnDeserializing]
		internal void OnDeserializingMethod(StreamingContext context)
		{
			//if we don't create a new list here the old items get lazy loaded and added to
			//then all items get updated.  This means that delete item don't get deleted!
					_filters = new List<Filter> ();
		}

			private IList<Filter> _filters;



		public string LogoURL { get; set; }
		public string TopBarColour { get; set; }
		public string BottomBarColour { get; set; }
		public string LoadingURL { get; set; }

		public DateTime LoadAllDate { get; set; }
		public DateTime LoadAreasDate { get; set; }
		public DateTime LoadFiltersDate { get; set; }
		public DateTime LoadTownsDate { get; set; }

		public string PinShadowURL { get; set; }

		public string ImagesPath { get; set; }
		public string AssetsPath { get; set; }

		public double CenterLatitude { get; set; }
		public double CenterLongitude { get; set; }

		public double RadiusMiles { get; set; }
		public string ButtonColour { get; set; }
		public string ButtonPressedColour { get; set; }
		public string TextColour { get; set; }
		public DateTime LastUpdated { get; set; }
		public DateTime LastDeleted { get; set; }

		[Ignore]
		public IList<Filter> Filters
		{
			get{
				if(_filters == null)
					_filters = FilterService.GetFilters();

				return _filters;
			}
			set{ _filters = value;}
		}


		// Retained to maintain compatibility with shared codebase - unused in NVG systems
		private string _imageHandlerUrl = String.Empty;
		[Ignore]
		public string ImageHandlerUrl {
			get {
				if (_imageHandlerUrl == null) {
					_imageHandlerUrl = string.Empty;
				}
				return _imageHandlerUrl;
			}
			set {
				_imageHandlerUrl = value;
			}
		}

		public string TrailAbout { get; set; }
		public string TrailTravel { get; set; }
		public string TrailHelp { get; set; }

		private bool? _shouldCheckForUpdates;
		public bool ShouldCheckForUpdates {
			get{
				if (_shouldCheckForUpdates == null)
					_shouldCheckForUpdates = true;
				return (bool)_shouldCheckForUpdates;
			}
			set {
				_shouldCheckForUpdates = value;
			}
		}

		private bool? _shouldShowLocation;
		public bool ShouldShowLocation {
			get {
				if (_shouldShowLocation == null)
					_shouldShowLocation = true;
				return (bool)_shouldShowLocation;
			}
			set {
				_shouldShowLocation = value;
			}
		}

		private bool? _shouldShareOnVisited;
		public bool ShouldShareOnVisited {
			get {
				if (_shouldShareOnVisited == null)
					_shouldShareOnVisited = true;
				return (bool)_shouldShareOnVisited;
			}
			set {
				_shouldShareOnVisited = value;
			}
		}

		public string FeedbackUrl { get; set; }
		*/
	}
}

