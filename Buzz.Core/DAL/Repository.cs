using System;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;

using Buzz.Core.Models;
using Buzz.Core.Contracts;
using Buzz.Core.DAL.SQLite;

using System.IO;
using Buzz.Core.Utils;



namespace Buzz.Core.DAL {
	/// <summary>
	/// TaskDatabase builds on SQLite.Net and represents a specific database, in our case, the MWC DB.
	/// It contains methods for retreival and persistance as well as db creation, all based on the 
	/// underlying ORM.
	/// </summary>
	public class Repository : SQLiteConnection {
		protected static Repository Connection = null;
		protected static string dbLocation;

        static object locker = new object ();
		
		/// <summary>
		/// Initializes a new instance of the <see cref="MWC.DL.Repository"/> Repository. 
		/// if the database doesn't exist, it will create the database and all the tables.
		/// </summary>
		/// <param name='path'>
		/// Path.
		/// </param>
		protected Repository (string path) : base (path)
		{
			this.Execute("PRAGMA foreign_keys=ON;");
			//"REFERENCES filter(Id) ON DELETE CASCADE"
			/*
			// create the tables - based on Locations app
			CreateTable<Config>();
			CreateTable<Entry>();
			CreateTable<EntryImage>();
			CreateTable<Filter>();
			CreateTable<FilterSub>();
			CreateTable<SearchConfig>();

			// Create tables - specific to Trails
			CreateTable<PointOfInterest>();
			CreateTable<Trail>();
			CreateTable<TrailImage>();
			CreateTable<Waypoint>();
			CreateTable<WaypointImage>();

			// FK
			CreateTable<Entry_FilterSub> ();
			CreateTable<SearchConfig_FilterSub>();
			*/
		}

		static Repository ()
		{
			// set the db location
			dbLocation = DatabaseFilePath;
			
			// instantiate a new db
			Connection = new Repository(dbLocation);
		}
		
		public static string DatabaseFilePath {
			get { 
#if SILVERLIGHT
				var path = "LocationsDb.db3";
#else

#if __ANDROID__
            string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); ;
#else
			// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
			// (they don't want non-user-generated data in Documents)
			string documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal); // Documents folder
			string libraryPath = Path.Combine (documentsPath, "../Library/");
#endif
			var path = Path.Combine (libraryPath, "LocationsDb.db3");
#endif		
			return path;	
}
		}

		public static IList<T> GetItems<T> () where T : class, IEntity, new()
		{
            lock (locker) {
				return  Connection.Table<T>().ToList ();
            }
		}

		public static IList<T> GetAllOrdered<T,U>(Expression<Func<T, U>> orderExpr) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Table<T>().OrderBy(orderExpr).ToList();
			}
		}


		public class IntWrapper
		{
			public int value{get;set;}
		}

		public static IEnumerable<int> GetItemIds<T> () where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Query<IntWrapper>(string.Format("Select id as value from {0}", typeof(T).Name), new object[0]).Select(w => w.value).ToList();
			}
		}

		public static IList<int> GetItemIds<T>(Expression<Func<T,bool>> predExpr) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Table<T>().Where(predExpr).Select<T,int>(e => e.Id).ToList();
			}	
		}




		public static IList<T> GetItemsWhere<T> (Expression<Func<T, bool>> predExpr) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Table<T>()
					.Where(predExpr).ToList();	
			}
		}

		public static IList<T> WhereOrdered<T, U> (Expression<Func<T, bool>> predExpr, Expression<Func<T, U>> orderExpr) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Table<T>()
					.Where(predExpr).OrderBy(orderExpr).ToList();	
			}
		}

		
		public static IList<T> WhereOrdered<T, U, V> (Expression<Func<T, bool>> predExpr, Expression<Func<T, U>> orderExpr, Expression<Func<T, V>> orderExpr2) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Table<T>()
					.Where(predExpr).OrderBy(orderExpr).OrderBy(orderExpr2).ToList();	
			}
		}

		public static T GetItem<T> (int id) where T : class, IEntity, new()
		{
            lock (locker) {
                
                // ---
                //return (from i in me.Table<T> ()
                //        where i.ID == id
                //        select i).FirstOrDefault ();

                // +++ To properly use Generic version and eliminate NotSupportedException
                // ("Cannot compile: " + expr.NodeType.ToString ()); in SQLite.cs
                return Connection.Table<T>().FirstOrDefault(x => x.Id == id);
            }
		}

		public static T GetItemWhere<T> (Expression<Func<T, bool>> predExpr) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Table<T>()
					.Where(predExpr).ToList().FirstOrDefault();	
			}
		}

		public static T QuerySingle<T>( string query, params object[] args) where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Query<T>(query, args).FirstOrDefault();
			}
		}

	
		public static int InsertItem<T> (T item) where T : class, IEntity, new()
		{
            lock (locker) {
					item.UpdatedDate = DateTime.UtcNow;
					return Connection.Insert (item);
            }
		}
		/*
		public static int UpdateItem<T> (T item) where T : class, IEntity, new()
		{
			lock (locker) {
					return Connection.Update (item);
				}
		}
*/
		public static void UpdateItem<T> (T item) where T : class, IEntity, new()
		{
			lock (locker) {
				item.UpdatedDate = DateTime.UtcNow;
				Connection.Update(item);
			}
		}

		public static void SaveItem<T> (T item) where T : class, IEntity, new()
		{
			//warning, this will replace NOT update an existing item and so trigget cascade deletes
			lock (locker) {
				item.UpdatedDate = DateTime.UtcNow;
				Connection.InsertOrReplace(item);
			}
		}

		public static void SaveItems<T> (IEnumerable<T> items) where T : class, IEntity, new()
		{
			if(items == null)
				return;

            lock (locker) {

				//little bit dirty as not specific to entities and only for main update 
				//but these are column we don't want to hit in data update

				string[] excludeCols = new string[] { "Active","Favourite" };

				IEnumerable<int> itemIds = Repository.GetItemIds<T>();

                Connection.BeginTransaction ();

                foreach (T item in items) {

					try{
					if(itemIds.Contains(item.Id))
					{
						//bypassed Repository.UpdateItem to use diff overload of update
						item.UpdatedDate = DateTime.UtcNow;
						Connection.Update(item, excludeCols);
					}
					else
					{
						Repository.InsertItem(item);
					}
					}
					catch(Exception ex)
					{
						LogHelper.LogDebug("failed to insert {0} : {1}. Exception: {2}", typeof(T).Name, item.Id, ex.Message);
					}
                }
                Connection.Commit ();
            }
		}

		public static int DeleteItem<T>(int id) where T : class, IEntity, new()
		{
            lock (locker) {
                return Connection.Delete<T> (new T () { Id = id });
            }
		}

		public static void DeleteItems<T> (IEnumerable<int> ids) where T : class, IEntity, new()
		{
			if(ids == null)
				return;
			
			lock (locker) {

				Connection.BeginTransaction ();
				
				foreach (int id in ids) {
					Connection.Delete<T>(id);	
				}
				
				Connection.Commit ();
			}
		}
		
		public static void ClearTable<T>()  where T : class, IEntity, new()
		{
            lock (locker) {
                Connection.Execute (string.Format ("delete from \"{0}\"", typeof (T).Name));
            }
		}
		
		// helper for checking if database has been populated
		public static int CountTable<T>() where T : Buzz.Core.Contracts.IEntity, new ()
		{
            lock (locker) {
				string sql = string.Format ("select count (*) from \"{0}\"", typeof (T).Name);
				var c = Connection.CreateCommand (sql, new object[0]);
				return c.ExecuteScalar<int>();
            }
		}

		public static int NonQuery( string query, params object[] args)
		{
			lock (locker) {
				return Connection.Execute(query, args);
			}
		}

	

		public static IList<T> TableQuery<T>(string query, params object[] args)  where T : class, IEntity, new()
		{
			lock (locker) {
				return Connection.Query<T>(query, args);
			}
		}

		public class StringWrapper
		{
			public string value{get;set;}
		}

		/// <summary>
		/// Tables the string query.
		/// </summary>
		/// <returns>The string query.</returns>
		/// <param name="query">Query must specify the return value as 'value'</param>
		/// <param name="args">Arguments.</param>
		public static IList<string> StringQuery(string query, params object[] args)
		{
			lock (locker) {
				return Connection.Query<StringWrapper>(query, args).Select(w => w.value).ToList();;
			}
		}
	
	}
}