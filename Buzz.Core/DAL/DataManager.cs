using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Buzz.Core.Models;

namespace Buzz.Core.DAL
{
	public class DataManager
	{
		public DataManager ()
		{
		}

		#region Config

		public static Config GetConfig()
		{
			// This ID could be 0 or 1 (or anything else for that matter) depending on how the DB has been set up.
			return Repository.GetItem<Config>(0);
		}

		public static void SaveConfig(Config config)
		{
			Repository.SaveItem (config);
		}

		#endregion


		/*
		#region Trails

		public static IList<Trail>GetTrails()
		{
			return Repository.WhereOrdered<Trail,int,int>( i => true, u => u.Priority, u => u.Id);
		}

		public static Trail GetTrail(int id)
		{
			return Repository.GetItem<Trail> (id);
		}

		public static IList<TrailImage> GetImagesForTrail(int id)
		{
			return Repository.WhereOrdered<TrailImage,int, int>(i => i.TrailId == id, u => u.Priority, u => u.Id);
		}

		public static IList<Waypoint> GetWaypointsForTrail(int id)
		{
			IList<Waypoint> waypoints = Repository.WhereOrdered<Waypoint,int, int>(i => i.TrailId == id, u => u.Priority, u => u.Id);

			int numberInTrailSequence = 1;
			foreach(Waypoint waypoint in waypoints)
			{
				waypoint.NumberInTrailSequence = numberInTrailSequence;
				numberInTrailSequence++;
			}

			return waypoints;
		}

		public static IList<TrailImage> GetTrailImages()
		{
			return Repository.GetItems<TrailImage>();
		}

		public static void SaveTrail(Trail trail)
		{
			Repository.UpdateItem(trail);
		}

		#endregion

		#region Waypoints

		public static Waypoint GetWaypoint(int id)
		{
			return Repository.GetItem<Waypoint> (id);
		}

		public static IList<WaypointImage> GetImagesForWaypoint(int id)
		{
			return Repository.WhereOrdered<WaypointImage,int, int>(i => i.WaypointId == id, u => u.Priority, u => u.Id);
		}

		public static IList<PointOfInterest> GetPointsOfInterestForWaypoint(int id)
		{
			return Repository.WhereOrdered<PointOfInterest,int, int>(i => i.WaypointId == id, u => u.Priority, u => u.Id);
		}

		public static void SaveWaypoints(IList<Waypoint> waypoints)
		{
			Repository.SaveItems (waypoints);
		}

		public static IList<WaypointImage> GetWaypointImages()
		{
			return Repository.GetItems<WaypointImage>();
		}

		public static IList<Waypoint> GetWaypoints()
		{
			return Repository.GetItems<Waypoint>();
		}

		public static IList<Waypoint> GetWaypointsWithAudio()
		{
			return Repository.GetItemsWhere<Waypoint>(o => o.AudioFile != "");
		}

		#endregion

		#region Filters

		public static IList<FilterSub> GetSubCategoriesListFilters()
		{
			return Repository.WhereOrdered<FilterSub, int>(f => f.Priority < 0, fs => fs.Priority);
		}

		public static IList<FilterSub> GetFilterSubs(int filterId)
		{
			return Repository.GetItemsWhere<FilterSub>(fs => fs.FilterId == filterId);
		}

		public static IList<FilterSub> GetAllFilterSubs()
		{
			return Repository.GetItems<FilterSub>();
		}

		public static IList<Filter> GetFilters()
		{
			return Repository.GetItems<Filter>();
		}

		public static FilterSub GetFilterSub(int id)
		{
			return Repository.GetItemsWhere<FilterSub> (fs => fs.Id == id).FirstOrDefault();
		}

		public static void DeActivateFilterSubs()
		{
			Repository.NonQuery("Update FilterSub set active = 0", string.Empty);
		}

		public static void SetActiveFilterSub(string code)
		{
			Repository.NonQuery("Update Filtersub set active = 1 Where code = ?1", code);
		}

		public static void SetActiveFilterSub(int id)
		{
			Repository.NonQuery("Update Filtersub set active = 1 Where id = ?1", id);
		}

		public static void SetActiveFilterSubsForSearch(int searchConfigId)
		{
			Repository.NonQuery("Update FilterSub set active = 1 WHERE id IN (SELECT FilterSubId FROM SearchConfig_FilterSub WHERE SearchConfig_FilterSub.SearchConfigId = ?1)", searchConfigId);
		}

		public static void SetActiveVirtualFilterSubs(int parentId)
		{
			Repository.NonQuery("Update FilterSub set active = 1 WHERE id IN (SELECT FilterSubId FROM Category_FilterSub WHERE Category_FilterSub.CategoryId = ?1)", parentId);
		}

		public static void ToggleFilterSub(int id)
		{
			Repository.NonQuery("Update Filtersub set active = NOT active Where id = ?1", id);
		}
	

		#endregion




		#region Entries

		public static Entry GetEntry(int entryId)
		{
			return Repository.GetItem<Entry> (entryId);
		}

		public static IList<FilterSub> GetFilterSubsForEntryByFilter(int entryId, string filterCode)
		{
			string query = @"SELECT filtersub.* FROM filtersub inner join
				entry_filtersub on filtersub.id = entry_filtersub.filtersubid inner join
					filter on filtersub.filterid = filter.id 
					WHERE filter.code = ?1 AND entry_filtersub.entryid = ?2";
			return Repository.TableQuery<FilterSub> (query, filterCode, entryId);
		}

		public static IList<FilterSub> GetFilterSubsForEntry(int entryId)
		{
			IList<Entry_FilterSub> entryFilterSubs = Repository.GetItemsWhere<Entry_FilterSub> (o => o.EntryId == entryId);
			IList<FilterSub> filterSubs = entryFilterSubs.Select(o => o.FilterSub).ToList();
			filterSubs = filterSubs.OrderBy (o => o.Priority).ToList();
			return filterSubs;
		}

		public static IList<Entry> GetEntries()
		{
			return Repository.GetItems<Entry> ();
		}

		public static IList<Entry> GetEntries(bool favouritesOnly, int? areaId, bool applyFilters )
		{
			return GetEntries(favouritesOnly, areaId, applyFilters, null, null);
		}

		public static IList<Entry> GetEntries(bool favouritesOnly, int? areaId, DateTime? searchStartDate, DateTime? searchEndDate )
		{
			return GetEntries(favouritesOnly, areaId, false, searchStartDate, searchEndDate);
		}

		public static IList<Entry> GetEntries(bool favouritesOnly, int? areaId, bool applyFilters, DateTime? searchStartDate, DateTime? searchEndDate )
		{
			int parameterCount = 1;
			List<object> parameters = new List<object>();
			
			
			StringBuilder query = new StringBuilder(@"SELECT Distinct entry.* FROM entry 
													inner join entry_categorysub on entry.id = entry_categorysub.entryid  
													inner join categorysub on entry_categorysub.categorysubid = categorysub.id ");

		
			if(searchStartDate.HasValue && searchEndDate.HasValue)
			{
				query.AppendLine(" inner join event on entry.id = event.entryid ");
			}



			string whereclause = " WHERE  (categorysub.active = 1 OR (select count(*) from categorysub where active = 1)  = 0)";

				
				if(applyFilters)
				{
				whereclause +=   @" AND entry.id in (
						
						select id from (
						select entryid as id, count(*) as filtermatches from entry_filtersub 
						inner join filtersub on entry_filtersub.filtersubId = filtersub.id
						where filtersub.active = 1
						group by entryid
						
						) where filtermatches = (select count(*) from filtersub where active = 1)
						) OR  (select count(*) from filtersub where active = 1)  = 0 
						";
					}

			
			string orderclause = " ORDER BY entry.Name ASC";
			
			
			if(favouritesOnly)
			{
				whereclause += " AND entry.favourite = 1 ";
			}
			
			if(areaId.HasValue)
			{
				query.AppendLine(" inner join entry_area on entry.id = entry_area.entryid ");
				
				whereclause += string.Format(" AND entry_area.areaid = ?{0} ", parameterCount);
				
				parameters.Add(areaId);
				parameterCount++;
			}
			
			if(searchStartDate.HasValue && searchEndDate.HasValue)
			{
				whereclause += string.Format("  AND event.startdate >= ?{0} AND event.startdate < ?{1}  ", parameterCount, parameterCount + 1);
				parameters.Add(searchStartDate.Value);
				parameters.Add(searchEndDate.Value);
				parameterCount++;
				parameterCount++;
			}
			
			
			query.AppendLine(whereclause);
			query.AppendLine(orderclause);

			return  Repository.TableQuery<Entry>( query.ToString(), parameters.ToArray());
		}

		public static IList<EntryImage> GetImagesForEntry(int id)
		{
			return Repository.WhereOrdered<EntryImage,int, int>(i => i.EntryId == id, u => u.Priority, u => u.Id);
		}

		public static IEnumerable<EntryImage> GetEntryImagesForSearch(IEnumerable<int> entryIds, int maxImages)
		{
			string query = @"SELECT * FROM entryimage  
								 WHERE entryimage.entryid IN ({0}) 
 								ORDER BY RANDOM() LIMIT ?1";
			query = string.Format(query, string.Join (",",entryIds));

			return Repository.TableQuery<EntryImage>(query, maxImages);

		}

		public static void SaveEntry(Entry entry)
		{
			Repository.UpdateItem(entry);
		}

		public static IEnumerable<string> GetEntryImages()
		{
			return Repository.StringQuery("select url as value from entryimage", new object[0]);
			//return Repository.GetItems<EntryImage>().Select(e => e.Url).Distinct().ToList();
		}

		public static IEnumerable<string> GetEntryImagesForCaching()
		{
			//TODO: Revert this filetype to B for BrowseImage once we have BrowseImages to use
			return Repository.StringQuery ("select url as value from entryimage where filetype = ?", "D");
			//return Repository.GetItemsWhere<EntryImage>(e => e.Priority == 1).Select(e => e.Url).Distinct().ToList();
		}

		public static IEnumerable<string> GetTrailImagesForCaching()
		{
			//TODO: Revert this filetype to B for BrowseImage once we have BrowseImages to use
			return Repository.StringQuery ("select url as value from trailimage where filetype = ?", "D");
		}

		public static IEnumerable<string> GetWaypointImagesForCaching()
		{
			//TODO: Revert this filetype to B for BrowseImage once we have BrowseImages to use
			return Repository.StringQuery ("select url as value from waypointimage where filetype = ?", "D");
		}

		public static IEnumerable<string> GetAudioForCaching()
		{
			IEnumerable<string> trailAudioUrls = Repository.StringQuery ("select AudioFile as value from Trail");
			IEnumerable<string> waypointAudioUrls = Repository.StringQuery ("select AudioFile as value from Waypoint");
			List<string> bothTypesOfAudioUrls = trailAudioUrls.ToList ();
			bothTypesOfAudioUrls.AddRange(waypointAudioUrls);
			return bothTypesOfAudioUrls;
		}

		#endregion

		#region PointsOfInterest

		public static IList<PointOfInterest> GetPointsOfInterest()
		{
			return Repository.GetItems<PointOfInterest>();
		}

		#endregion

		*/
	}
}

