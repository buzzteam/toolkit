using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;


using Buzz.Core.Models;
using RestSharp;
using Buzz.Core.Utils;

namespace Buzz.Core.SAL
{
	public class ApiClient
	{
		readonly string _apiKey;
		readonly string _apiSecret;
		readonly string _apiBaseUrl;

		public ApiClient(string baseUrl) {
			_apiBaseUrl = baseUrl;
		}
	    
		public ApiClient(string apiUrl, string apiKey, string apiSecret) {
			_apiKey = apiKey;
			_apiBaseUrl = apiUrl;
			_apiSecret = apiSecret;
		}
		
		public T Execute<T>(RestRequest request) where T : new()
		{

			RestClient client = new RestClient ();
			client.BaseUrl = _apiBaseUrl;
			//	client.ClearHandlers();
			//	client.Timeout = 30000;
			// Our JSON is wrapped in XML - let's make sure everything fits
			//RestSharp.Deserializers.IDeserializer deserializer = new RestSharp.Deserializers.JsonInXmlWrapperNetDeserializer();
			//client.AddHandler("text/xml", deserializer);

			//	RestSharp.Deserializers.IDeserializer deserializer = new RestSharp.Deserializers.JsonNetDeserializer();
			//client.AddHandler("application/json", deserializer);

			//request.AddParameter("ApiKey", _apiKey, ParameterType.UrlSegment); // used on every request
			//	request.AddParameter("Client1", _apiKey, ParameterType.GetOrPost);
			//	request.AddParameter("Client2", _apiSecret, ParameterType.GetOrPost);

			//	AddAuthHeaders (client, request);		

			var response = client.Execute<T>(request);
			
			if (response.ErrorException != null)
			{
				throw response.ErrorException;
			}
			return response.Data;
		}

		private void AddAuthHeaders(RestClient client, RestRequest request)
		{
			var timestamp = DateTime.UtcNow.ToString("U");
			var verb = request.Method.ToString ();
			//var path = client.BuildUri (request).LocalPath.ToLower();
			var resource = client.BuildUri (request).PathAndQuery;

			string message = string.Format("{0}{1}{2}",verb,timestamp,resource);
			string hash = ComputeHash(_apiSecret, message);
			request.AddParameter ("x-auth-timestamp", timestamp, ParameterType.HttpHeader);
			request.AddParameter("x-auth-signature", hash, ParameterType.HttpHeader);
		}


		private string ComputeHash(string hashedPassword, string message)
		{
			var key = Encoding.UTF8.GetBytes(hashedPassword.ToUpper());
			string hashString;

			using (var hmac = new HMACSHA256(key))
			{
				var hash = hmac.ComputeHash(Encoding.UTF8.GetBytes(message));
				hashString = Convert.ToBase64String(hash);
			}

			return hashString;
		}




		public Config GetConfig()
		{
			var request = new RestRequest();
			request.Resource = "dmsXiPhoneGetAppConfigJSON";
			request.RootElement = "config";

			return Execute<Config>(request);
		}


		public FBUser GetFBUser()
		{
			var request = new RestRequest();
			//	request.Resource = "helstonrailway";
			request.RootElement = "FBUser";
			
			return Execute<FBUser>(request);
		}

		/*
		public List<Entry> GetEntries(DateTime lastUpdated)
		{
			var request = new RestRequest();
			request.Resource = "dmsXiPhoneGetEntriesJSON";
			request.RootElement = "entries";

			// This AddMinutes fails if last updated is completely zero
			//request.AddParameter("UpdatedAfter", lastUpdated.AddMinutes(-5).ToString("yyyy-MM-dd HH:mm"), ParameterType.GetOrPost);
			request.AddParameter("UpdatedAfter", lastUpdated.ToString("yyyy-MM-dd HH:mm"), ParameterType.GetOrPost);

			request.AddParameter("IDs", "All", ParameterType.GetOrPost);

			return Execute<List<Entry>>(request);
		}
	
		public List<DeletedEntry> GetDeletedEntries(DateTime lastUpdated)
		{
			var request = new RestRequest();
			request.Resource = "dmsXiPhoneGetDeletedEntriesJSON";
			request.RootElement = "entries";
			
			request.AddParameter("DeletedAfter",lastUpdated.ToString("yyyy-MM-dd HH:mm"), ParameterType.GetOrPost);
			
			return Execute<List<DeletedEntry>>(request);
		}

		public Deletion GetDeletion(DateTime lastUpdated)
		{
			var request = new RestRequest();
			request.Resource = "dmsXiPhoneGetDeletedEntriesJSON";

			request.AddParameter("DeletedAfter",lastUpdated.ToString("yyyy-MM-dd HH:mm"), ParameterType.GetOrPost);

			return Execute<Deletion>(request);
		}

		public List<Trail> GetTrails()
		{
			var request = new RestRequest();
			request.Resource = "dmsXiPhoneGetTrailsJSON";
			request.RootElement = "trails";

			request.AddParameter("IDs", "All", ParameterType.GetOrPost);

			return Execute<List<Trail>>(request);
		}

		public static T GetFromJson<T>(string data, string rootElement)
		{
			IRestResponse dummyRepsonse = new RestResponse{
				 Content = data
			};

			RestSharp.Deserializers.JsonNetDeserializer deserializer = new RestSharp.Deserializers.JsonNetDeserializer();
			deserializer.RootElement = rootElement;
			return deserializer.Deserialize<T>(dummyRepsonse);
		}
*/
	}
}

