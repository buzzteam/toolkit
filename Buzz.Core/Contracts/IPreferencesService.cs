using System;

namespace Buzz.Core.Contracts
{
	public interface IPreferencesService
	{
		 string GetSettingString(string key);
		 DateTime GetSettingDate(string key);
		 void SetSettingString(string key, string value);
		 void SetSettingDate(string key, DateTime value);

	}
}

