using System;
using System.Collections.Generic;

namespace Buzz.Core.Contracts
{
	public interface IImageMetaData
	{
		string Url { get; }
		int Priority { get; }
		string FileType { get; }
	}
}

