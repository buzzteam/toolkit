using System;

namespace Buzz.Core.Contracts
{
	public interface IExpirable
	{
		DateTime UpdatedDate {get;set;}
	}
}

