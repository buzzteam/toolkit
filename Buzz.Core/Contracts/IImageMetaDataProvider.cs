using System;
using System.Collections.Generic;

namespace Buzz.Core.Contracts
{
	public interface IImageMetaDataProvider
	{
		IList<IImageMetaData> ImageMetaData { get; }
	}
}