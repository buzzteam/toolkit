using System;
using System.Collections.Generic;

namespace Buzz.Core.Contracts
{
	public interface IImageCacher
	{
		IEnumerable<string> CachedImageNames{get;}
		string CachedName(string input);
		void CacheImage(string url);
		void DeleteFilesFromCache (IEnumerable<string> storeNames);
	}



}

