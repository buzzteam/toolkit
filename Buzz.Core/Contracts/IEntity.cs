using System;

using Buzz.Core.DAL.SQLite;

namespace Buzz.Core.Contracts
{
	 public interface IEntity
    {	[PrimaryKey]
        int Id { get; set;}
		DateTime UpdatedDate {get;set;}
    }
}

