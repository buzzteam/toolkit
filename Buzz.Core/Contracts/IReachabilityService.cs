﻿using System;

namespace Buzz.Core.Contracts
{
	public enum NetworkStatus {
		NotReachable,
		ReachableViaCarrierDataNetwork,
		ReachableViaWiFiNetwork
	}

	public interface IReachabilityService
	{
		NetworkStatus InternetConnectionStatus ();
	}
}

