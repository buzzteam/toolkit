using System;

namespace Buzz.Core.Contracts
{
	public interface ISystemService
	{
		decimal GetPixelDensity(); 

		#if __ANDROID__
		object GetApplicationContext();
		#endif

		bool CanLaunchApp(string app);
	}
}

