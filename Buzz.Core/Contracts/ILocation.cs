using System;

namespace Buzz.Core.Contracts
{
	public interface ILocation
	{
		double Latitude {get; set;}
		double Longitude {get; set;}
	}
}

