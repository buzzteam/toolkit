using System;
using System.Linq;
using Buzz.Core.Models;
using Buzz.Core.DAL;


namespace Buzz.Core.Services
{
	public static class ConfigService
	{
		private static Config _config;
		public static Config Config{
			get{
				if(_config == null)
				{
					_config =  DataManager.GetConfig();
				}
				return _config;
			}
		}

		public static void ReloadConfig()
		{
			_config = DataManager.GetConfig ();
		}

		/*
		public static void UpdateShouldCheckForUpdates(bool state)
		{
			_config.ShouldCheckForUpdates = state;
			DataManager.SaveConfig (_config);
		}

		public static void UpdateShouldShowLocation(bool state)
		{
			_config.ShouldShowLocation = state;
			DataManager.SaveConfig (_config);
		}

		public static void UpdateShouldShareOnVisted(bool state)
		{
			_config.ShouldShareOnVisited = state;
			DataManager.SaveConfig (_config);
		}
		*/
	}
}

