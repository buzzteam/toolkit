using System;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using System.Timers;

using Newtonsoft.Json.Linq;

using Buzz;
using Buzz.Core.SAL;
using Buzz.Core.Contracts;
using Buzz.Core.Models;
using Buzz.Core.DAL;
using Buzz.Core.Utils;


namespace Buzz.Core.Services
{
	public class UpdateFinishedEventArgs : EventArgs
	{
		public bool Success {get;set;}
		public UpdateFinishedEventArgs (bool success)
		{
			this.Success = success;
		}
	}

	public static class UpdateService
	{
		private static System.Timers.Timer _nextUpdateTimer;
		private static object _locker = new object();
		private static ApiClient _client {get; set;}
		private static DateTime _updateDate{get;set;}



		public static bool IsUpdating{get;set;}
		public static bool IsLoadingSeedData{get;set;}

		public static event EventHandler OnUpdateStarted = delegate {};

		public delegate void UpdateFinished(object sender, UpdateFinishedEventArgs e);
		public static event  UpdateFinished OnUpdateFinished;

		public static event EventHandler LoadSeedDataStarted = delegate {};
		public static event EventHandler LoadSeedDataFinished = delegate {};


		static UpdateService()
		{

		}

		/*
		public static bool HasData {
			get {
				return DataManager.GetTrails ().Count () != 0;
			}
		}

		public static void UpdateFromFile(string data)
		{
			lock(_locker)
			{
				LogHelper.LogDebug("Loading seed data");

				if(LoadSeedDataStarted != null)
				{
					LoadSeedDataStarted(null, new EventArgs());
				}

				IsLoadingSeedData = true;

				try
				{
					JObject jroot = JObject.Parse(data);

					Config config = LocationsApiClient.GetFromJson<Config>(data, "Config");
					List<Filter> filters = LocationsApiClient.GetFromJson<List<Filter>>(data, "Filters");
					List<Entry> entries = LocationsApiClient.GetFromJson<List<Entry>>(data, "Entries");

					DateTime updatedDate = jroot.Properties().FirstOrDefault(p => p.Name == "UpdatedDate").Value.ToObject<DateTime>();

					//DateTime updatedDate = LocationsApiClient.GetFromJson<DateTime>(data,"UpdatedDate");

					if(config == null || entries == null)
					{
						throw new NullReferenceException("LocationApiResult Null");
					}
					
					Repository.SaveItem<Config>(config);
					ProcessFilters(filters);				
					ProcessEntries(entries);

					IPreferencesService preferencesHelper = ServiceContainer.Resolve<IPreferencesService>();
					preferencesHelper.SetSettingDate("LastUpdated" , updatedDate);

					//HasData = true;
				}
				catch(Exception ex)
				{
					//if our seed data fails to load then we're a bit stuffed really!
					LogHelper.LogDebug(ex.Message);

				}
				finally
				{
					IsLoadingSeedData = false;

					if(LoadSeedDataFinished != null)
					{
						LoadSeedDataFinished(null, new EventArgs());
					}
				}
				LogHelper.LogDebug("Finished loading seed data {0}", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.ffff"));
			}
		}

		public static void CheckUpdateStatus()
		{
			//this method should only ever be called externally on the apps load however just in case
			//we don't want this running whilst an update is already in progress

			// Only run the update if the user wants us to...
			if (ConfigService.Config == null || ConfigService.Config.ShouldCheckForUpdates) {

				lock (_locker) {
					IPreferencesService preferencesHelper = ServiceContainer.Resolve<IPreferencesService> ();

					_updateDate = DateTime.UtcNow;
					LogHelper.LogDebug ("Update beginning at: {0}", _updateDate.ToString ("yyyy-MM-dd HH:mm:ss.ffff"));

					TimeSpan cacheAge = _updateDate - preferencesHelper.GetSettingDate ("LastUpdated");
				
					LogHelper.LogDebug (string.Format ("Cache date(utc): {0} ;  Cache age: {1}", preferencesHelper.GetSettingDate ("LastUpdated").ToString ("yyyy-MM-dd HH:mm:ss"), cacheAge.TotalMilliseconds));

					if (cacheAge > Constants.MaxCacheAge) {
						Update ();

						LogHelper.LogDebug ("Caching images");
						ThreadPool.QueueUserWorkItem (state => UpdateService.CacheImages ());
						ThreadPool.QueueUserWorkItem (state => UpdateService.CacheAudio ());
					}
				}
			} else {
				LogHelper.LogDebug ("User has updates disabled");
			}
		}

		private static void Update()
		{
			lock(_locker)
			{
				if(OnUpdateStarted != null)
				{
					OnUpdateStarted(null, new EventArgs());
				}

				IPreferencesService preferencesHelper = ServiceContainer.Resolve<IPreferencesService>();

				LogHelper.LogInfo("Starting Update Sync");

				IsUpdating = true;
				
				LocationsApiClient _client  = new LocationsApiClient(Constants.LocationApiUrl,Constants.LocationsApiKey, Constants.LocationsApiSecret);

				try {
					LogHelper.LogDebug("Processing Data");
					
					Config config = _client.GetConfig();

					if (config != null)
					{
						// Copy existing config items
						if (ConfigService.Config != null)
						{
							config.ShouldCheckForUpdates = ConfigService.Config.ShouldCheckForUpdates;
							config.ShouldShowLocation = ConfigService.Config.ShouldShowLocation;
						}

						Repository.SaveItem<Config>(config);
						ConfigService.ReloadConfig();
					}

					//DateTime updatedDate = DateTime.Now.AddDays(-2000);
					DateTime updatedDate = preferencesHelper.GetSettingDate("LastUpdated");

					List<Filter> filters = config.Filters.ToList();
					List<Entry> entries = _client.GetEntries(updatedDate);
					
					List<Trail> trails = _client.GetTrails();

					Deletion deletion = _client.GetDeletion(updatedDate);

					ProcessFilters(filters);

					LogHelper.LogInfo("Deleting {0} Entries", deletion.DeletedEntryIds.Count);
					Repository.DeleteItems<Entry>(deletion.DeletedEntryIds);

					LogHelper.LogInfo("Deleting {0} Waypoints", deletion.DeletedWaypointIds.Count);
					Repository.DeleteItems<Waypoint>(deletion.DeletedWaypointIds);
					
					ProcessEntries(entries);

					ProcessTrails(trails);

					preferencesHelper.SetSettingDate("LastUpdated" , _updateDate);

					UpdateComplete(true);
				}
				catch(Exception ex)
				{
					// In case of a total failure, we should still store the last updated date as NVG data can be
					// ligitimately empty, and therefore cause a null ref exception (for example when there are no new Entries)
					preferencesHelper.SetSettingDate("LastUpdated" , _updateDate);

					LogHelper.LogDebug(ex.Message);
					UpdateComplete(false);
				}
				finally
				{
					
				}
			}
		}
	
		private static void ProcessFilters(List<Filter> filters)
		{
			LogHelper.LogDebug ("Processing Filters");
			InsertUpdateEntities(filters);

			// We need to explicitly set the FilterId in FilterSubs by refference because the Id is not included in NVG data
			foreach (Filter filter in filters) {
				foreach (FilterSub filterSub in filter.FilterSubs) {
					filterSub.FilterId = filter.Id;
					// PoIs are off by default
					filterSub.Active = false;
				}
				InsertUpdateEntities(filter.FilterSubs.ToList());
			}
		}

		private static void ProcessEntries(List<Entry> entries)
		{
			LogHelper.LogDebug("Processing {0} entries", entries.Count);

			//IEnumerable<int> favIds = Repository.GetItemIds<Entry>(e => e.Favourite == true);
			//entries.ForEach(e => e.Favourite = favIds.Contains(e.Id));

			InsertUpdateEntities(entries);

			foreach (Entry entry in entries) {
				foreach (EntryImage entryImage in entry.Images) {
					LogHelper.LogInfo("Adding EntryImage {0} to Entry {1}", entryImage.Url, entry.Id);
					entryImage.EntryId = entry.Id;
				}
				InsertUpdateEntities(entry.Images.ToList());
			}

			List<Entry_FilterSub> entryFilterSubs = entries.SelectMany(
				entry => entry.FilterSubIds,(entry, FilterSubId) => new { entry.Id , FilterSubId }).Select(
				entryIdFilterSubId => new Entry_FilterSub(){Id = int.Parse(string.Format("{0}{1}", entryIdFilterSubId.Id, entryIdFilterSubId.FilterSubId)) , EntryId = entryIdFilterSubId.Id, FilterSubId = entryIdFilterSubId.FilterSubId }).ToList();
			InsertUpdateEntities(entryFilterSubs);

			//clean up time
			//delete any un-updated items where their parent entry HAS been updated
			LogHelper.LogDebug("Entry Cleanup: {0}", _updateDate);
			//Repository.NonQuery ("DELETE FROM Category WHERE UpdatedDate < ?1", _updateDate);
			Repository.NonQuery ("DELETE FROM EntryImage WHERE EntryId IN (SELECT Id FROM Entry WHERE Entry.UpdatedDate >= ?1) AND EntryImage.UpdatedDate < ?1", _updateDate);
			Repository.NonQuery ("DELETE FROM Entry_FilterSub WHERE EntryId IN (SELECT Id FROM Entry WHERE Entry.UpdatedDate >= ?1) AND Entry_FilterSub.UpdatedDate < ?1", _updateDate);
		}

		private static void ProcessTrails(List<Trail> trails)
		{
			InsertUpdateEntities(trails);

			foreach (Trail trail in trails)
			{
				Trail storedTrail = DataManager.GetTrail (trail.Id);

				foreach (Waypoint waypoint in trail.Waypoints) {
					Waypoint storedWaypoint = DataManager.GetWaypoint (waypoint.Id);
					waypoint.TrailId = trail.Id;
					if (storedWaypoint != null) {
						waypoint.VisitedId = storedWaypoint.VisitedId;
					}
				}
				InsertUpdateEntities(trail.Waypoints.ToList());

				foreach (TrailImage trailImage in trail.Images) {
					LogHelper.LogInfo("Adding TrailImage {0} to Trail {1}", trailImage.Url, trail.Id);

					if (storedTrail != null) {
						TrailImage storedImage = (TrailImage)storedTrail.Images.FirstOrDefault(o => o.Url == trailImage.Url);
						if (storedImage != null) {
							trailImage.Id = storedImage.Id;
						}
					}

					trailImage.TrailId = trail.Id;
				}
				InsertUpdateEntities(trail.Images.ToList());

				foreach (Waypoint waypoint in trail.Waypoints) {
					Waypoint storedWaypoint = DataManager.GetWaypoint (waypoint.Id);

					foreach (PointOfInterest pointOfInterest in waypoint.PointsOfInterest) {
						pointOfInterest.WaypointId = waypoint.Id;
					}
					InsertUpdateEntities(waypoint.PointsOfInterest.ToList());

					foreach (WaypointImage waypointImage in waypoint.Images) {
						LogHelper.LogInfo("Adding WaypointImage {0} to Waypoint {1}", waypointImage.Url, waypoint.Id);

						if (storedWaypoint != null) {
							WaypointImage storedImage = (WaypointImage)storedWaypoint.Images.FirstOrDefault(o => o.Url == waypointImage.Url);
							if (storedImage != null) {
								waypointImage.Id = storedImage.Id;
							}
						}

						waypointImage.WaypointId = waypoint.Id;
					}
					InsertUpdateEntities(waypoint.Images.Cast<WaypointImage>().ToList());
				}
			}
		}

		private static void InsertUpdateEntities<T>(List<T> entities) where T : class, IEntity, new()
		{

			//we need to break up large updates so as not to lock the repository
			for (int i = 0; i < Math.Ceiling((decimal)entities.Count/20m); i++) {
				Repository.SaveItems(entities.Skip(i * 20).Take(20));
			}

			//Repository.SaveItems(entities);

			//IExpirable items, these are item where we get the whole lot everytime
			//Set the updateddate and then after delete any that have not been updated to remove deleted ones
			if(typeof(IExpirable).IsAssignableFrom(typeof(T)))
			{
				string delExpired = string.Format ("delete from {0} where UpdatedDate < ?", typeof(T).Name);
				Repository.NonQuery(delExpired, _updateDate);
			}	

		}

		public static void UpdateComplete(bool success)
		{
		
			if(OnUpdateFinished != null)
				OnUpdateFinished(null, new UpdateFinishedEventArgs(success));	

			IsUpdating = false;
		}

		public static void CacheImages()
		{
			IImageCacher imageManager =	ServiceContainer.Resolve<IImageCacher>();

			IEnumerable<string> cachedUrls = imageManager.CachedImageNames;

			List<string> entryImagesToCache = DataManager.GetEntryImagesForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList();
			entryImagesToCache = entryImagesToCache.Where(i => !cachedUrls.Contains(imageManager.CachedName(i))).ToList();
			LogHelper.LogDebug("Starting Entry image caching count: {0}", entryImagesToCache.Count());
			entryImagesToCache.ForEach(i => imageManager.CacheImage(i));

			List<string> trailImagesToCache = DataManager.GetTrailImagesForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList();
			trailImagesToCache = trailImagesToCache.Where(i => !cachedUrls.Contains(imageManager.CachedName(i))).ToList();
			LogHelper.LogDebug("Starting Trail image caching count: {0}", trailImagesToCache.Count());
			trailImagesToCache.ForEach(i => imageManager.CacheImage(i));

			List<string> waypointImagesToCache = DataManager.GetWaypointImagesForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList();
			waypointImagesToCache = waypointImagesToCache.Where(i => !cachedUrls.Contains(imageManager.CachedName(i))).ToList();
			LogHelper.LogDebug("Starting Waypoint image caching count: {0}", waypointImagesToCache.Count());
			waypointImagesToCache.ForEach(i => imageManager.CacheImage(i));


			//copy to a md5's list ready for comparison against cache to delete old images.
			List<string> imagesElligableToCache = new List<string> ();

			//entry list images	
			imagesElligableToCache.AddRange (DataManager.GetEntryImagesForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList());
			//entry detail images
			imagesElligableToCache.AddRange (DataManager.GetEntryImages().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList());

			imagesElligableToCache.AddRange (DataManager.GetTrailImagesForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList());
			imagesElligableToCache.AddRange (DataManager.GetWaypointImagesForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatImageDirectUrlString(i))).ToList());

			List<string> imagesElligableToCacheMD5 = imagesElligableToCache.Select(url => imageManager.CachedName(url)).ToList();

			IEnumerable<string> imagesToDelete = cachedUrls.Except(imagesElligableToCacheMD5).ToList();
			imageManager.DeleteFilesFromCache (imagesToDelete);
		}

		public static void CacheAudio()
		{
			IAudioCacher audioManager =	ServiceContainer.Resolve<IAudioCacher>();

			IEnumerable<string> cachedUrls = audioManager.CachedAudioNames;

			List<string> audioToCache = DataManager.GetAudioForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatAudioDirectUrlString(i))).ToList();
			audioToCache = audioToCache.Where(i => !cachedUrls.Contains(audioManager.CachedName(i))).ToList();
			LogHelper.LogDebug("Starting audio chaching count: {0}", audioToCache.Count());
			audioToCache.ForEach(i => audioManager.CacheAudio(i));

			//copy to a md5's list ready for comparison against cache to delete old images.
			List<string> audioElligableToCache = new List<string> ();

			//entry list images	
			audioElligableToCache.AddRange (DataManager.GetAudioForCaching().Select(i => Uri.UnescapeDataString(LocationsHelpers.FormatAudioDirectUrlString(i))).ToList());

			List<string> audioElligableToCacheMD5 = audioElligableToCache.Select(url => audioManager.CachedName(url)).ToList();

			IEnumerable<string> audioToDelete = cachedUrls.Except(audioElligableToCacheMD5).ToList();
			audioManager.DeleteFilesFromCache (audioToDelete);
		}

		public static void ClearAllData()
		{
			Repository.ClearTable<Entry> ();
			Repository.ClearTable<EntryImage> ();
			Repository.ClearTable<Filter> ();
			Repository.ClearTable<FilterSub> ();
			Repository.ClearTable<SearchConfig> ();

			Repository.ClearTable<PointOfInterest> ();
			Repository.ClearTable<Trail> ();
			Repository.ClearTable<TrailImage> ();
			Repository.ClearTable<Waypoint> ();
			Repository.ClearTable<WaypointImage> ();

		}
		*/
	}
}