using System;

namespace Buzz.Core.Exceptions
{
	public class NullImageLoaderViewException : Exception
	{
		public NullImageLoaderViewException()
			: base() { }

		public NullImageLoaderViewException(string message)
			: base(message) { }

		public NullImageLoaderViewException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public NullImageLoaderViewException(string message, Exception innerException)
			: base(message, innerException) { }

		public NullImageLoaderViewException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}

	public class NullImageLoadingIndicatorViewException : Exception
	{
		public NullImageLoadingIndicatorViewException()
			: base() { }

		public NullImageLoadingIndicatorViewException(string message)
			: base(message) { }

		public NullImageLoadingIndicatorViewException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public NullImageLoadingIndicatorViewException(string message, Exception innerException)
			: base(message, innerException) { }

		public NullImageLoadingIndicatorViewException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}

	public class NullViewPagerException : Exception
	{
		public NullViewPagerException()
			: base() { }

		public NullViewPagerException(string message)
			: base(message) { }

		public NullViewPagerException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public NullViewPagerException(string message, Exception innerException)
			: base(message, innerException) { }

		public NullViewPagerException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}

	public class NullFragmentManagerException : Exception
	{
		public NullFragmentManagerException()
			: base() { }

		public NullFragmentManagerException(string message)
			: base(message) { }

		public NullFragmentManagerException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public NullFragmentManagerException(string message, Exception innerException)
			: base(message, innerException) { }

		public NullFragmentManagerException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}

	public class NullImageSliderViewAdapterException : Exception
	{
		public NullImageSliderViewAdapterException()
			: base() { }

		public NullImageSliderViewAdapterException(string message)
			: base(message) { }

		public NullImageSliderViewAdapterException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public NullImageSliderViewAdapterException(string message, Exception innerException)
			: base(message, innerException) { }

		public NullImageSliderViewAdapterException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}

	public class BadDeQueueTypeException : Exception
	{
		public BadDeQueueTypeException()
			: base() { }

		public BadDeQueueTypeException(string message)
			: base(message) { }

		public BadDeQueueTypeException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public BadDeQueueTypeException(string message, Exception innerException)
			: base(message, innerException) { }

		public BadDeQueueTypeException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}

	public class ImagesAlreadySetException : Exception
	{
		public ImagesAlreadySetException()
			: base() { }

		public ImagesAlreadySetException(string message)
			: base(message) { }

		public ImagesAlreadySetException(string format, params object[] args)
			: base(string.Format(format, args)) { }

		public ImagesAlreadySetException(string message, Exception innerException)
			: base(message, innerException) { }

		public ImagesAlreadySetException(string format, Exception innerException, params object[] args)
			: base(string.Format(format, args), innerException) { }
	}
}

