using System;
using System.Diagnostics;

namespace Buzz.Core.Utils
{
	public static class LogHelper
	{
		[System.Diagnostics.Conditional("DEBUG")]
		public static void LogDebug(string format, params object[] arg)
		{
			Debug.WriteLine(string.Format(format, arg));
		}

		[System.Diagnostics.Conditional("DEBUG")]
		public static void LogDebug(string message)
		{
			Debug.WriteLine(message);
		}

		public static void LogInfo(string message)
		{
		 	Console.WriteLine(message);
		}

		public static void LogInfo(string format, params object[] arg)
		{
			Console.WriteLine(string.Format(format, arg));
		}
	}
}

