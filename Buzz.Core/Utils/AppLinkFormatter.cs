using System;
using System.Text.RegularExpressions;
using Buzz.Core.SAL;
using Buzz.Core.Contracts;

namespace Buzz.Core.Utils
{
	public class AppLinkFormatter
	{
		public const string TwitterAppPrefix = "twitter://";
		public const string TwitterUserModifier = "user?screen_name=";
		public const string TwitterLinkFormat = @".*twitter[^/]*/(?<username>[^/?]+)";
		#if __ANDROID__
		public const string TwitterLaunchName = "com.twitter.android";
		#else
		public const string TwitterLaunchName = "twitter://";
		#endif


		public const string FacebookAppPrefix = "fb://";
		public const string FacebookUserModifier = "profile/";
		public const string FacebookUserLinkFormat = @".*facebook[^/]*/[^/?]*";
		public const string FacebookPagesLinkFormat = @".*facebook[^/]*/pages/[^/]*/(?<userid>[^/?]+)";
		#if __ANDROID__
		public const string FacebookLaunchName = "com.facebook.katana";
		#else
		public const string FacebookLaunchName = "fb://";
		#endif
		public const string FacebookLinkPreference = "prefence_facebook_link";
		public const string FacebookLaunchPreference = "prefence_facebook_launch";




		public static string GetFacebookLaunchLink(string link, int entryId) {

			IPreferencesService preferencesHelper = ServiceContainer.Resolve<IPreferencesService>();
			ISystemService systemService = ServiceContainer.Resolve<ISystemService>();

			if (!systemService.CanLaunchApp (FacebookLaunchName))
					return link;

			if (preferencesHelper.GetSettingString(FacebookLinkPreference + entryId) == link) {
				return preferencesHelper.GetSettingString (FacebookLaunchPreference + entryId);
			} else {
				string launchLink;
				if (link.Contains ("pages")) {
					Regex linkPattern = new Regex (FacebookPagesLinkFormat);
					Match match = linkPattern.Match (link);

					string userid = match.Groups ["userid"].Value;

					launchLink = FacebookAppPrefix + FacebookUserModifier + userid;
				} else {
					Regex userPattern = new Regex (FacebookUserLinkFormat);
					Match match = userPattern.Match (link);

					string graph = match.ToString().Replace ("www", "graph");
					FBUser fbUser;
					try {
						ApiClient apiClient = new ApiClient (graph);
						fbUser = apiClient.GetFBUser();

						if (fbUser.Id == 0)
							launchLink = link;
						else
							launchLink = FacebookAppPrefix + FacebookUserModifier + fbUser.Id;
					} catch (Exception) {
						launchLink = link;
					}

				}

				preferencesHelper.SetSettingString (FacebookLinkPreference + entryId, link);
				preferencesHelper.SetSettingString (FacebookLaunchPreference + entryId, launchLink);

				return launchLink;
			}
		}

		public static string FormatGenericLink(string link, string launchName, string appPrefix, string webLinkFormat, string modifier)
		{
			ISystemService systemService = ServiceContainer.Resolve<ISystemService>();
			if (!systemService.CanLaunchApp (launchName))
				return link;


			Regex linkPattern = new Regex (webLinkFormat);
			Match match = linkPattern.Match (link);

			string username = match.Groups ["username"].Value;

			return appPrefix + modifier + username;
		}

		public static string GetTwitterLaunchLink(string link) {
			return FormatGenericLink(link, TwitterLaunchName, TwitterAppPrefix, TwitterLinkFormat, TwitterUserModifier);	
		}
	}

	public class FBUser
	{
		public long Id { get; set; }
	}
}

